var fs = require('fs');
var env = process.env.NODE_ENV;

var config = {
	'LOCAL': {
		'api': 'http://localhost:3006/api'
	},
	'PRD': {
		'api': 'xxx'
	},
	'DEV': {
		'api': 'xxx'
	}
}

fs.writeFileSync('./src/config/config.json', JSON.stringify(config[env]));
