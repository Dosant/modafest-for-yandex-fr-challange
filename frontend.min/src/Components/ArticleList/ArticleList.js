import React from 'react';
import './ArticleList.css';
import ArticleListItem from '../ArticleListItem/ArticleListItem';

function ArticleList({ articles, isLoading, loadMore }) {
	return (
		<div className="article-list">
			{articles.map((article) => {
				return (
					<ArticleListItem key={article.id} article={article} />
				)
			}) }
			{ !isLoading ?
				<div className="center-button-container">
					<button className="button button-outline button-fc no-margin" onClick={loadMore}> Показать Ещё</button>
				</div> :
				null
			}
		</div>
	);
}

export default ArticleList;
