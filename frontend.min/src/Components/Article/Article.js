import './Article.css';
import React from 'react';
import { Link } from 'react-router';
import renderHTML from 'react-render-html';


function Article({ article, isLoading }) {
	if (isLoading) {
		return (<div></div>);
	}

	return (
		<article className="article">
			<header className="article-header">
				<p className="article-header__tag">
					{article.tags[0].name}
				</p>
				<h3 className="article-header__title">
					{renderHTML(article.title) }
				</h3>
				<p className="aritlce-header__excerpt">
					{article.excerpt}
				</p>
			</header>
			<content className="article-content">
				{renderHTML(article.fullText) }
			</content>
			<footer className="article-footer">
				<p>
					Теги <br/>
					{renderTags(article.tags) }
				</p>
				<div className="center-button-container">
					<Link to='/' className="button button-outline button-fc no-margin">Перейти в журнал</Link>
				</div>
			</footer>
		</article>
	);
}

function renderTags(tags) {
	return tags.map((tag) => {
		return (
			<Link to={'/t/' + tag.id} key={tag.id}>
				{tag.name}
			</Link>
		)
	});
}

export default Article;
