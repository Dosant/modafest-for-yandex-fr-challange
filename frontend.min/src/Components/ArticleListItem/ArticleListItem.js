import React from 'react';
import { Link } from 'react-router';
import moment from 'moment';
import renderHTML from 'react-render-html';
import './ArticleListItem.css';

function ArticleListItem({ article }) {
	return (
		<div className="article-list-item">
			<Link to={'/a/' + article.id}>
				{(article.featuredImage ? renderWithImage(article) : renderNoImage(article)) }
			</Link>
		</div>
	);
}

function renderWithImage(article) {
	return (
		<div className="row">
			<div className="col-xs-12 col-sm-4">
				<img className="article-list-item__image" src={article.featuredImage.url} role="presentation"/>
			</div>
			<div className="col-xs-12 col-sm-8">
				<h4>{renderHTML(article.title) }</h4>
				<p>{article.excerpt}</p>
				<p className="no-margin">{renderTags(article.tags) }</p>
				<p className="no-margin">{renderDate(article.publishedAt) }</p>
			</div>
		</div>
	);
}


function renderNoImage(article) {
	return (
		<div className="row">
			<div className="col-xs-12">
				<h4>{article.title}</h4>
				<p>{article.excerpt}</p>
				<p className="no-margin">{renderTags(article.tags) }</p>
				<p className="no-margin">{renderDate(article.publishedAt) }</p>
			</div>
		</div>
	);
}

function renderTags(tags) {
	return (
		<span className="article-list-item__tags"><strong>Теги: </strong> {parseTags(tags) }</span>
	)
}

function renderDate(date) {
	return (
		<span>{moment(date).fromNow() }</span>
	)
}

function parseTags(tags) {
	return tags.map((tag) => tag.name).join(' ');
}


export default ArticleListItem;
