import React, { Component } from 'react';
import { getArticle } from '../../services/api';
import Article from '../Article/Article';

class ArticleContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: true,
			article: {}
		}
	}

	componentDidMount() {
		this.loadArticle();
	}

	loadArticle() {
		return getArticle(this.props.params.id)
			.then((article) => {
				this.setState({
					isLoading: false,
					article: article
				});
			});
	}

	render() {
		return (
			<div className='content-container'>
				<Article article={this.state.article} isLoading={this.state.isLoading}/>
			</div>
		)
	}
}

// <ArticleList article={this.state.article} isLoading={this.state.isLoading} />

export default ArticleContainer;
