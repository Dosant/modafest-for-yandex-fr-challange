import './Header.css';
import React from 'react';
import { Link } from 'react-router';
import logo from '../../assets/images/modafestLogoBlack.png';


function Header() {
    return (
        <div className="Header">
			<Link to='/'>
                <div className="Header__logo-wrapper">
                    <img src={logo} className="Header__logo" alt="logo" />
                </div>
            </Link>
        </div>
    );
}

export default Header;
