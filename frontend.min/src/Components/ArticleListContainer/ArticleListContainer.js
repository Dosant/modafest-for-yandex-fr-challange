import React, { Component } from 'react';
import ArticleList from '../ArticleList/ArticleList';
import { getArticles } from '../../services/api';

class ArticleListContainer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			page: 1,
			isLoading: true,
			articles: []
		}

		this.loadMore = this.loadMore.bind(this);
	}

	componentDidMount() {
		this.loadArticles();
	}

	loadArticles() {
		return getArticles(this.state.page)
			.then((articles) => {
				this.setState({
					page: ++this.state.page,
					articles: this.state.articles.concat(articles),
					isLoading: false
				});
			})
	}

	loadMore() {
		if (this.state.isLoading) { return; }
		return this.loadArticles();
	}

	render() {
		return (
			<div className='content-container'>
				<ArticleList articles={this.state.articles} loadMore={this.loadMore} isLoading={this.state.isLoading} />
			</div>
		)
	}
}

export default ArticleListContainer;
