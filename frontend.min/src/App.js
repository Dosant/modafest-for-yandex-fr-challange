import React, { Component } from 'react';
import { Router, Route, Redirect, IndexRoute, browserHistory } from 'react-router';
import './App.css';

import Layout from './Components/Layout/Layout';
import ArticleListContainer from './Components/ArticleListContainer/ArticleListContainer';
import TagArticleListContainer from './Components/TagArticleListContainer/TagArticleListContainer';
import ArticleContainer from './Components/ArticleContainer/ArticleContainer';


class App extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route path='/' component={Layout}>
          <IndexRoute component={ArticleListContainer} />
          <Route path='/t/:id' component={TagArticleListContainer} />
          <Route path='/a/:id' component={ArticleContainer} />
        </Route>
        <Redirect from="*" to="/" />
      </Router>
    );
  }
}

export default App;
