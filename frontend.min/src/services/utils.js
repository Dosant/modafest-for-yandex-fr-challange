import atob from 'atob';
export function b64_to_utf8(str) {
	return decodeURIComponent(escape(atob(str)));
}
