import { api } from '../config/config';
import { b64_to_utf8 } from './utils';

export function getArticles(page) {
	return fetch(api + '/sections/magazine?page=' + page)
		.then((res) => res.json())
		.then((res) => {
			return res.cover.concat(res.articles);
		});
}

export function getArticlesWithTag(tagId, page) {
	return fetch(api + '/tags/' + tagId + '?page=' + page)
		.then((res) => res.json())
		.then((res) => {
			return res.cover.concat(res.articles);
		});
}

export function getArticle(id) {
	return fetch(api + '/articles/' + id)
		.then((res) => res.json())
		.then((res) => {
			res.fullText = b64_to_utf8(res.fullText);
			return res;
		});
}
