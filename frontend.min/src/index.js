import 'es5-shim/es5-shim';
import 'es5-shim/es5-sham';
import promise from 'es6-promise';
promise.polyfill();
import 'fetch-ie8';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'milligram';
import 'bootstrap-grid';
import './index.css';

import moment from 'moment';
moment.locale('ru');


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
