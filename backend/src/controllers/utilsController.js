var fetch = require('node-fetch');
var htmlToText = require('html-to-text');
var btoa = require('btoa');


module.exports = {
    fetchFCPosts,
    fetchFCPostsByIds,
    fetchFCPost
};

function getSelfUrl(req) {
    return req.protocol + "://" + req.get('host');
}

function fetchFCPosts(req, res, next) {
    const token = req.headers['x-access-token'];


    const offset = req.query.offset || 0;
    const limit = req.query.limit || 10;
    const url = getSelfUrl(req) + '/api/articles';
    const postsUrl = `xxx`;
    var result = {
        success: [],
        failure: []
    };
    fetch(postsUrl)
        .then((res) => res.json())
        .then(response => {
            const posts = response;
            const promise = posts.reduce((promise, post) => {
                return promise.then(() => {
                    console.log('Fetching post: ' + post.title.rendered);
                    return fetchPost(post, url, token)
                        .then(() => {
                            console.log('Success fetching post: ' + post.title.rendered);
                            result.success.push(post.title.rendered);
                        })
                        .catch(() => {
                            console.log('Failure fetching post: ' + post.title.rendered);
                            result.failure.push(post.title.rendered);
                            // continue fetching
                            return Promise.resolve(true);
                        });
                });
            }, Promise.resolve());
            return promise;
        })
        .then(() => {
            res.json(result);
        })
        .catch(next);
}

function fetchFCPost(req, res, next) {
    const token = req.headers['x-access-token'];

    const id = req.params.id;
    const url = getSelfUrl(req) + '/api/articles';
    const postUrl = 'xxx' + id;
    fetch(postUrl)
        .then((res) => res.json())
        .then(post => {
            return fetchPost(post, url, token);
        })
        .then(response => {
            res.json(response);
        })
        .catch(next);
}

function fetchFCPostsByIds(req, res, next) {
    const token = req.headers['x-access-token'];

    const ids = req.body.ids;
    const url = getSelfUrl(req) + '/api/articles';
    const result = {
        success: [],
        failure: []
    }

    ids.reduce((promise, id) => {
        const postUrl = 'xxx' + id;
        return promise
            .then(() => {
                return fetch(postUrl)
                    .then((res) => res.json())
                    .then(post => {
                        return fetchPost(post, url, token)
                            .then((res) => {
                                console.log('Success fetching post: ' + post.title.rendered);
                                result.success.push({
                                    newId: res.article.id,
                                    oldId: post.id,
                                    title: post.title.rendered
                                });
                            })
                            .catch(() => {
                                console.log('Failure fetching post: ' + post.title.rendered);
                                result.failure.push({
                                    oldId: post.id,
                                    title: post.title.rendered
                                });
                                // continue fetching
                                return Promise.resolve(true);
                            });
                    })
            })

    }, Promise.resolve())
        .then(response => {
            console.log(JSON.stringify(result, null, 2));
            res.json(response);
        })
        .catch(next);
}

function fetchPost(post, url, token) {
    const id = post.slug;
    const title = post.title.rendered;
    const createdAt = post.date;
    const modifiedAt = post.modified;
    const publishedAt = post.date;
    const isPublished = true;
    const type = 'REGULAR';
    const excerpt = htmlToText.fromString(post.excerpt.rendered, { wordwrap: false }).slice(0, 140) + '...';
    const fullText = btoa(unescape(encodeURIComponent(post.content.rendered)));
    const sections = [{
        id: "magazine",
        name: "Журнал"
    }];

    const article = {
        id,
        title,
        createdAt,
        modifiedAt,
        excerpt,
        type,
        fullText,
        sections,
        author: 'Fashion Collection',
        publishedAt,
        isPublished,
        oldId: post.id
    };

    const tags = fetchCategories(post.categories)
        .then(tags => {
            article.tags = tags;
        });

    const image = fetchImage(post.featured_media)
        .then(image => {
            if (image.id && image.url) {
                article.featuredImage = image;
            }
        });

    const author = fetchAuthor(post.author)
        .then((author) => {
            article.author = author;
        });
    return Promise.all([tags, image, author])
        .then((res) => {
            return article;
        })
        .catch((res) => {
            return article;
        })
        .then((article) => {
            return postArticle(article, url, token);
        });
}

function fetchCategories(categoriesIds) {
    const categoriesUrl = 'xxx';
    return Promise.all(categoriesIds.map((id) => {
        return fetch(categoriesUrl + id)
            .then((res) => res.json())
            .then((res) => ({ id: res.slug, name: res.name }));
    }));
}

function fetchImage(imageId) {
    const imageUrl = 'xxx' + imageId;
    return fetch(imageUrl)
        .then((res) => res.json())
        .then((image) => {
            var url
            //var index = 'http//'.length + 1, result;
            try {
                const sizes = image['media_details'].sizes;
                const size = sizes['large'] || sizes['medium_large'] || sizes['full'];
                url = size ? size['source_url'] : image.source_url;
                //result = url.splice(index, 0, 'i1.wp.com/');
                return { id: image.id, url: url };
            } catch (err) {
                url = image.source_url;
                //result = url.splice(index, 0, 'i1.wp.com/');
                return { id: image.id, url: url };
            }
        });
}

function fetchAuthor(authorId) {
    const authorUrl = 'xxx' + authorId;
    return fetch(authorUrl)
        .then((res) => res.json())
        .then((author) => author.name);
}

function postArticle(article, url, token) {
    return fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'x-access-token': token
        },
        body: JSON.stringify(article)
    })
        .then(checkStatus)
        .then((res) => res.json());
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    } else {
        var error = new Error(response.statusText);
        error.response = response;
        throw error;
}
}