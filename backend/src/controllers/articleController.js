var $q = require('q')
var Article = require('../models/articleModel.js')
var ArticleType = require('../models/articleTypeModel.js')
var sectionController = require('./sectionController')
var tagController = require('./tagController')

var articleController = {
    getArticles: getArticles,
    getNews: getNews,
    getBrand: getBrand,
    getLookbook: getLookbook,
    getArticleById: getArticleById,
    postArticle: postArticle,
    editArticleById: editArticleById,
    deleteArticleById: deleteArticleById,
    searchArticles: searchArticles,
    postArticleType: postArticleType
}

module.exports = articleController

function getArticles(req, res, next) {
    var query = Article.find()
    query.select('-_id -__v -fullText')
        .sort({ createdAt: 'desc' })
        .skip(req.params.pagination.skip)
        .limit(req.params.pagination.limit)
        .exec()
        .then((results) => {
            Article.count({}, function (err, count) {
                res.set('Total', count)
                res.json(results);
            });
        }, next);
}

function getNews(req, res, next) {
    getArticlesByType('NEWS', req, res, next);
}

function getBrand(req, res, next) {
    getArticlesByType('BRAND', req, res, next);
}

function getLookbook(req, res, next) {
    getArticlesByType('LOOKBOOK', req, res, next);
}

function getArticlesByType(type, req, res, next) {
    var articles = Article
        .find({ type: type, isPublished: true })
        .sort({ publishedAt: 'desc' })
        .select('-_id -__v -fullText')
        .skip(req.params.pagination.skip)
        .limit(req.params.pagination.limit)
        .exec();

    var metadata = ArticleType.find({ id: type })
        .exec();

    Promise.all([articles, metadata])
        .then((result) => {
            res.json({
                articles: result[0],
                metadata: result[1][0].metadata
            });
        }, next);
}

function getArticleById(req, res) {
    Article.findOne({ id: req.params.id }, function (err, result) {
        if (err) {
            console.log(err)
            res.json(err)
        } else {
            res.json(result)
        }
    })
}

function deleteArticleById(req, res) {
    Article.findOneAndRemove({ id: req.params.id }, function (err, result) {
        if (err || !result) {
            console.log(err)
            res.json(err)
        } else {
            // need to delete _id from tags & sections
            var _id = result._id
            return $q.all([
                sectionController.deleteArticleIdFromSections(_id, result.sections),
                tagController.deleteArticleIdFromTags(_id, result.tags)
            ])
        }
    })
        .then(function () {
            res.end('Article deleted')
        })
        .catch(function (err) {
            res.end('Error: could not delete article:' + err)
        })
}

function postArticle(req, res, next) {
    var entry = req.body
    return Promise.all([sectionController.updateSections(entry.sections),
        tagController.updateTags(entry.tags)])
        .then(function (results) {
            return saveArticle(results[0], results[1])
        })
        .then(function (results) {
            var _id = results.savedArticle._id
            var sections = results.sections
            var tags = results.tags
            return Promise.all([
                sectionController.addArticleIdToSections(_id, sections),
                tagController.addArticleIdToTags(_id, tags)
            ]).then(() => results.savedArticle)
        })
        .then(function (article) {
            res.json({ message: 'Article saved successfully', article: article })
        })
        .catch(next)

    function saveArticle(sections, tags) {
        var articleSettings = {
            id: entry.id,
            title: entry.title,
            author: entry.author,
            excerpt: entry.excerpt,
            type: entry.type,
            fullText: entry.fullText,
            sections: sections,
            tags: tags,
            createdAt: entry.createdAt,
            modifiedAt: entry.modifiedAt,
            oldId: entry.oldId
        }

        // featuredImage
        if (entry.featuredImage) {
            articleSettings.featuredImage = entry.featuredImage
        }

        // if posted with published flag
        if (entry.isPublished) {
            articleSettings.isPublished = true
            articleSettings.publishedAt = entry.publishedAt || new Date()
        }
        var article = new Article(articleSettings)

        return article.save()
            .then((result) => {
                return {
                    savedArticle: result,
                    sections: sections,
                    tags: tags
                }
            })
    }
}

function editArticleById(req, res, next) {
    var entry = req.body

    // 1. findArticle
    // 2. remove it from sections and tags
    // 3. put it into sections and tags
    // 4. update
    var article

    var findArticle = $q.defer()
    Article.findOne({ id: req.params.id }, function (err, _article) {
        if (err) {
            next(err)
        }
        article = _article
        findArticle.resolve(article)
    })

    findArticle.promise.then(function () {
        return $q.all([sectionController.updateSections(entry.sections), tagController.updateTags(entry.tags)])
    }).then(function (result) {
        entry.sections = result[0]
        entry.tags = result[1]

        return $q.all([updateSections(article._id, result[0], article.sections), updateTags(article._id, result[1], article.tags)])
    }).then(function () {
        updateArticle(article, entry)
    })

    function updateSections(_id, newSections, oldSections) {
        if (!newSections) {
            return true
        }

        var deleteSections = oldSections.filter(function (oldSection) {
            return !newSections.find(function (newSection) {
                return oldSection.id === newSection.id
            })
        })
        var addSections = newSections.filter(function (newSection) {
            return !oldSections.find(function (oldSection) {
                return oldSection.id === newSection.id
            })
        })

        return $q.all([sectionController.addArticleIdToSections(_id, addSections), sectionController.deleteArticleIdFromSections(_id, deleteSections)])
    }

    function updateTags(_id, newTags, oldTags) {
        if (!newTags) {
            return true
        }

        var deleteTags = oldTags.filter(function (oldTag) {
            return !newTags.find(function (newTag) {
                return oldTag.id === newTag.id
            })
        })
        var addTags = newTags.filter(function (newTag) {
            return !oldTags.find(function (oldTag) {
                return oldTag.id === newTag.id
            })
        })

        return $q.all([tagController.addArticleIdToTags(_id, addTags), tagController.deleteArticleIdFromTags(_id, deleteTags)])
    }

    function updateArticle(articleModel, newArticle) {
        articleModel.id = newArticle.id || articleModel.id
        articleModel.title = newArticle.title || articleModel.title
        articleModel.author = newArticle.author || articleModel.author
        articleModel.excerpt = newArticle.excerpt || articleModel.excerpt
        articleModel.type = newArticle.type || articleModel.type
        articleModel.fullText = newArticle.fullText || articleModel.fullText
        articleModel.sections = newArticle.sections || articleModel.sections
        articleModel.tags = newArticle.tags || articleModel.tags
        articleModel.featuredImage = newArticle.featuredImage || articleModel.featuredImage
        articleModel.modifiedAt = new Date()

        // check published flag
        if (newArticle.isPublished && !articleModel.isPublished) {
            // publish article
            articleModel.isPublished = true
            articleModel.publishedAt = new Date()
        } else {
            articleModel.isPublished = newArticle.isPublished
        }

        articleModel.save(function (err, result) {
            if (err) {
                res.json(err)
            } else {
                res.json(result)
            }
        })
    }
}

function searchArticles(req, res, next) {
    var term = req.params.term
    Article.find({ $text: { $search: term } }, { score: { $meta: 'textScore' } })
        .sort({ score: { $meta: 'textScore' } })
        .exec((err, articles) => {
            if (err) {
                console.log(err)
                next(err)
            } else {
                res.json(articles)
            }
        })
}

// helper function
function postArticleType(req, res, next) {
    var articleTypeInfo = req.body
    var articleType = new ArticleType(articleTypeInfo)
    articleType.save()
        .then((result) => {
            res.json(result)
        }, next)
}
