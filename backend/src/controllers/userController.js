var User = require('../models/userModel');

var userController = {
    reset: reset,
    addUser: addUser
};

module.exports = userController;

function reset(req, res) {
    User.remove({}, function (err, _res) {
      res.json(_res || err)
    });
}

function addUser(req, res, next) {
  var newUser = new User({
    email: req.body.email,
    password: req.body.password
  })

  newUser.save()
    .then((_res) => {
      res.json(_res);
    }, next);
}
