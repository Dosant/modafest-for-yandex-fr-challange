var async = require("async");
var $q = require("q");

var Article = require('../models/articleModel.js');
var Tag = require('../models/tagModel.js');

var tagController = {
    getTags: getTags,
    getTagById: getTagById,
    updateTags:updateTags,
    addArticleIdToTags: addArticleIdToTags,
    deleteArticleIdFromTags: deleteArticleIdFromTags

};

module.exports = tagController;

function getTags(req, res) {
    Tag
        .find({})
        .select('-articles -_id -__v')
        .sort({priority: 'desc', count: 'desc'})
        .exec(function(err, results){
            if(err){
                console.log(err);
                res.json(err);
            } else {
                res.json(results);
            }
        });
}

function getTagById (req, res) {
    var tagId = req.params.id;
    Tag.findOne({id: tagId}, function(err, tag) {
        if (err) {
            res.json(err);
        } else {
            if (!tag) {
                res.status(404);
                res.end();
                return;
            }
            var _id = tag._id;

            // query articles
            Article
                .find({_id: {$in : tag.articles}, isPublished: true})
                .select('-__v -_id')
                .sort({createdAt: "desc"})
                .skip(req.params.pagination.skip)
                .limit(req.params.pagination.limit)
                .exec(function(err, results){
                    if(err){
                        console.log(err);
                        res.json(err);
                    } else {
                        res.set('Total', tag.count);
                        res.json({articles: results, cover: []});
                    }
                });
        }
    });
}

function updateTags(tags) {
    var defered = $q.defer();
    if (!tags) {
        defered.resolve(false);
        return defered.promise;
    }

    function getTagIdOrCreateTag(tag, done) {
        Tag.findOne({id: tag.id}, function(err, results) {
            if(err){
                done(err);
            } else {
                if (results) {
                    // get tag here
                    done(null, results);
                } else {
                    // need to create this tag first
                    var newTag = new Tag({
                        id: tag.id,
                        name: tag.name
                    });
                    newTag.save(function(err, results) {
                        done(null,results);
                    });
                }
            }
        });
    }
    async.map(tags, getTagIdOrCreateTag, function(err, results) {
        if(err) {
            defered.reject(err);
        } else {
            var tags = [];
            results.map(function(res) {
                tags.push({
                    _id: res._doc._id,
                    id: res._doc.id,
                    name: res._doc.name || ""
                });
            });
            defered.resolve(tags);
        }
    });
    return defered.promise;
}

function addArticleIdToTags(_id, tags) {
    if (tags.length === 0) {
        return true;
    }

    var defered = $q.defer();
    function addArticleId(tag, done) {
        Tag.findByIdAndUpdate(tag._id, { $push: {"articles": _id}, $inc:{count: 1}}, {"new": true}, function(err, results) {
            if (err) {
                done(error);
            } else {
                done(null, results);
            }
        });
    }
    async.map(tags, addArticleId, function(err, results) {
        if (err) {
            defered.reject(err);
        } else {
            defered.resolve(results);
        }
    });
    return defered.promise;
}

function deleteArticleIdFromTags(_id, tags) {
    if (tags.length === 0) {
        return true;
    }

    var defered = $q.defer();
    function deleteArticleId(tag, done) {
        Tag.findByIdAndUpdate(tag._id, { $pull: {"articles": _id}, $inc:{count: -1}}, function(err, results) {
            if (err) {
                done(error);
            } else {
                done(null, results);
            }
        });
    }
    async.map(tags, deleteArticleId, function(err, results) {
        if (err) {
            defered.reject(err);
        } else {
            defered.resolve(results);
        }
    });
    return defered.promise;
}
