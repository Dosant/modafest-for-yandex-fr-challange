var async = require('async')
var $q = require('q')

var Article = require('../models/articleModel.js')

var Section = require('../models/sectionModel.js')

var sectionController = {
    getSections: getSections,
    getSectionById: getSectionById,
    updateSections: updateSections,
    addArticleIdToSections: addArticleIdToSections,
    deleteArticleIdFromSections: deleteArticleIdFromSections,
    postSection: postSection,
    createSection: createSection,
    resetSections: resetSections
}

module.exports = sectionController

function getSections(req, res) {
    Section
        .find({})
        .select('-articles -_id -__v')
        .sort({ priority: 'desc' })
        .exec(function (err, results) {
            if (err) {
                console.log(err)
                res.json(err)
            } else {
                res.json(results)
            }
        })
}

// http://mongoosejs.com/docs/populate.html
// probobly populate is better?
// db.getCollection('sections').findOne({id: "section1"}, {articles: {$slice: 5}}) -- get first 5
// db.getCollection('sections').update({id: "section1"}, {$push : {articles: {$each: [], $sort: {createdAt: -1}}}}) -- keep it sorted O_o

function getSectionById(req, res, next) {
    const sectionId = req.params.id;
    var _section;

    Section.findOne({ id: sectionId })
        .then((section) => {
            if (!section) { return Promise.reject({status: 404, message: 'not found'} ); }
            _section = section;
            const regularArticles = Article
                .find({ _id: { $in: section.articles }, type: 'REGULAR', isPublished: true })
                .select('-__v -_id')
                .sort({ createdAt: 'desc' })
                .skip(req.params.pagination.skip)
                .limit(req.params.pagination.limit)
                .exec()

            const coverArticles = Article
                .find({ _id: { $in: section.articles }, type: 'COVER', isPublished: true })
                .select('-__v -_id')
                .sort({ createdAt: 'desc' })
                .skip(req.params.pagination.cover.skip)
                .limit(req.params.pagination.cover.limit)
                .exec()

            return Promise.all([regularArticles, coverArticles])
        })
        .then((results) => {
            res.set('Total', _section.count)
            res.json({
                articles: results[0],
                cover: results[1]
            });
        })
        .catch(next);
}

function updateSections(sections) {
    var defered = $q.defer()
    if (!sections) {
        defered.resolve(false)
        return defered.promise
    }

    function getSectionIdOrCreateSection(section, done) {
        Section.findOne({ id: section.id }, function (err, results) {
            if (err) {
                done(err)
            } else {
                if (results) {
                    // get section here
                    done(null, results)
                } else {
                    // need to create this section first
                    var newSection = new Section({
                        id: section.id,
                        name: section.name
                    })
                    newSection.save(function (err, results) {
                        if (err) {
                            done(err)
                        } else {
                            done(null, results)
                            // update count in section
                        }
                    })
                }
            }
        })
    }
    async.map(sections, getSectionIdOrCreateSection, function (err, results) {
        if (err) {
            defered.reject(err)
        } else {
            var sections = []
            results.map(function (res) {
                sections.push({
                    _id: res._doc._id,
                    id: res._doc.id,
                    name: res._doc.name || ''
                })
            })
            defered.resolve(sections)
        }
    })
    return defered.promise
}

function addArticleIdToSections(_id, sections) {
    if (sections.length === 0) {
        return true
    }

    var defered = $q.defer()

    function addArticleId(section, done) {
        Section.findByIdAndUpdate(section._id, { $push: { 'articles': _id }, $inc: { count: 1 } }, { 'new': true }, function (err, results) {
            if (err) {
                done(error)
            } else {
                done(null, results)
            }
        })
    }
    async.map(sections, addArticleId, function (err, results) {
        if (err) {
            defered.reject(err)
        } else {
            defered.resolve(results)
        }
    })
    return defered.promise
}

function deleteArticleIdFromSections(_id, sections) {
    if (sections.length === 0) {
        return true
    }

    var defered = $q.defer()
    function deleteArticleId(section, done) {
        Section.findByIdAndUpdate(section._id, { $pull: { 'articles': _id }, $inc: { count: -1 } }, function (err, results) {
            if (err) {
                done(error)
            } else {
                done(null, results)
            }
        })
    }
    async.map(sections, deleteArticleId, function (err, results) {
        if (err) {
            defered.reject(err)
        } else {
            defered.resolve(results)
        }
    })
    return defered.promise
}

function postSection(req, res) {
    if (req.body.id && req.body.name) {
        createSection({
            id: req.body.id,
            name: req.body.name
        }).then(function (result) {
            res.json(result)
        }).catch(function (err) {
            res.status('409').send(err)
        })
    } else {
        res.status(500).send('Params error')
    }
}

function createSection(section) {
    // check if section exists
    var defered = $q.defer()
    Section.findOne({ id: section.id }, function (err, results) {
        if (err) {
            defered.reject(err)
        } else {
            if (results) {
                // section exists
                defered.reject('Section with id: ' + section.id + ' Exists')
            } else {
                // create new section
                var newSection = new Section({
                    id: section.id,
                    name: section.name
                })
                newSection.save(function (err, results) {
                    if (err) {
                        defered.reject(err)
                    } else {
                        defered.resolve(results)
                    }
                })
            }
        }
    })
    return defered.promise
}

function resetSections() {
    return new Promise((resolve, reject) => {
        Section.remove({}, function (err) {
            if (err) {
                reject(err)
            } else {
                // Load default
                var sections = require('../services/defaultSections')
                var saveSection = function (section, done) {
                    var newSection = new Section(section)
                    newSection.save(function (err, results) {
                        if (err) {
                            done(err)
                        } else {
                            done(null, results)
                        }
                    })
                }

                async.map(sections, saveSection, function (err, results) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(results)
                    }
                })
            }
        })
    })
}
