var Image = require('../models/imageModel.js');
var btoa = require('btoa');
var rest = require('unirest');

var imageController = {
    getImages: getImages,
    postImage: postImage,
    deleteImage: deleteImage
};

module.exports = imageController;

var cloudinaryApi = "xxx"
var cloudinaryKey = {
    apiKey: "xxx",
    secretKey: "xxx"
};
var cloudinaryAuth = "Basic " + btoa(cloudinaryKey.apiKey + ':' + cloudinaryKey.secretKey);

function getImages (req, res) {
    var next_cursor = req.query.next_cursor || '';
    rest.get(cloudinaryApi + '/image')
        .header('Authorization', cloudinaryAuth)
        .query({
            next_cursor: next_cursor,
			max_results: 30
        })
        .end(function (response) {
            res.json(response.body);
        });
}

function deleteImage(req,res) {
    var public_ids = req.query.public_ids;
    rest.delete(cloudinaryApi + '/image/upload')
        .header('Authorization', cloudinaryAuth)
        .query({
            public_ids: public_ids
        })
        .end(function (response) {
            res.json(response.body);
        });
}


function postImage (req, res) {
    var newImage = new Image(req.body);
    newImage.save(function(err, results) {
        if (err) {
            res.json(err);
        } else {
            res.json(results);
        }
    });
}
