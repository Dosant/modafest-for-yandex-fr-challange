var User = require('../models/userModel');
var jwt = require('jsonwebtoken');
var secret = require('../config/config').secret;

var authController = {
    login: login
};

module.exports = authController;

function login (req, res) {
    User.findOne({
        email: req.body.email
    }, function (err, user) {
        // general error
        if (err) {
            res.status(401).end(err);
            return;
        }

        if (!user) {
            res.status(401);
            res.json({
                message: 'Authentication failed. User not found.'
            });
        } else {
            if (!user.validPassword(req.body.password)) {
                res.status(401);
                res.json({
                    message: 'Authentication failed. Wrong password.'
                });
            } else {
                // good to go
                var token = jwt.sign({email: user.email}, secret, {
                    expiresIn: 1440*60 // expires in 24 hours
                });

                res.json({
                    message: 'Successfully authenticated',
                    user: {
                        email: user.email
                    },
                    token: token
                });
            }
        }
    });
}
