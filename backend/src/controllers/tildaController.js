var fetch = require('node-fetch');
var tildaConfig = require('../config/config').tilda;
var TildaProjectModel = require('../models/tildaModel');

module.exports = {
    getPage: getPage,
    fetchTilda: fetchTilda
};

function getProjectUrl(projectId) {
    return `${tildaConfig.url}/getproject/?publickey=${tildaConfig.publickey}&secretkey=${tildaConfig.secretkey}&projectid=${projectId}`;
}

function getPagesListUrl(projectId) {
    return `${tildaConfig.url}/getpageslist/?publickey=${tildaConfig.publickey}&secretkey=${tildaConfig.secretkey}&projectid=${projectId}`;
}

function getPageUrl(pageId) {
    return `${tildaConfig.url}/getpage/?publickey=${tildaConfig.publickey}&secretkey=${tildaConfig.secretkey}&pageid=${pageId}`;
}

function isProjectExists(projectId) {
    return new Promise( (resolve) => {
        TildaProjectModel.findOne({projectId: projectId}, (err, result) => {
            if (result) {
                return resolve(result);
            } else {
                return resolve(new TildaProjectModel({projectId: projectId, files: {css: [], js: []}}));
            }
        });
    });
}

function fetchTilda(req, res, next) {
    var projectId = req.params.projectId || req.query.projectId || req.query.projectid;
    if (!projectId) {next('Unable to get projectId');}

    var tildaProject;
    isProjectExists(projectId)
        .then((_tildaProject) => {
            tildaProject = _tildaProject;
            return fetch(getProjectUrl(projectId));
        })
        .then((res) => res.json())
        .then(response => {
            if (response.status !== 'FOUND') {
                return Promise.reject('Project not found');
            }
            tildaProject.projectId = response.result.id;
            tildaProject.title = response.result.title;
            tildaProject.files.css = response.result.css;
            tildaProject.files.js = response.result.js;
            return response.result.id;
        })
        .then((projectId) => fetch(getPagesListUrl(projectId)))
        .then((res) => res.json())
        .then((response) => {
            if (response.status !== 'FOUND') {
                return Promise.reject('PagesList not found');
            }
            var pages = response.result;
            var fetchPages = pages.map((page) => {
                return fetch(getPageUrl(page.id))
                    .then((res) => res.json());
            });
            return Promise.all(fetchPages);
        })
        .then((response) => {
            if (response.some((res) => res.status !== 'FOUND')) {
                return Promise.reject('Some Page not found');
            }
            var pages = response.map((res) => res.result);
            tildaProject.pages = pages;
            return tildaProject;
        })
        .then(() => {
            tildaProject.save( (err, result) => {
                if (err) {
                    next(err);
                } else {
                    res.json(result);
                }
            });
        })
        .catch(next);
}

function getPage(req, res, next) {
    var projectId = req.params.projectId;
    var pageId = req.params.pageId;
    TildaProjectModel.findOne({projectId: projectId}, (err, result) => {
        if (err) {next(err);}
        var project = result._doc;
        var page = project.pages.filter((page) => page.id === pageId);
        if (page.length > 0) {
            project.page = page[0];
            res.json(project);
        } else {
            next('Page not found');
        }
    });
}