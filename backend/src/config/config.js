var args = process.argv;

//Check which mongo instance to use
//mongo lab herokeDev account
var mongoUrl = 'xxx';

//heroku add-on
if (process.env.MONGODB_URI) {
    mongoUrl = process.env.MONGODB_URI;
}

if (args.indexOf('--mongo') !== -1) {
    mongoUrl = args[args.indexOf('--mongo') + 1];
}

//local
if (args.indexOf('--use-local-mongo') !== -1 || args.indexOf('-l') !== -1) {
    mongoUrl = 'mongodb://localhost:27017/fcollection-dev-test';
}
var port = 3006; // default port
if (args.indexOf('--port') !== -1) {
	port = args[args.indexOf('--port') + 1];
}

if (process.env.NODE_ENV === 'test') {
    port = 3333;
    mongoUrl = 'mongodb://localhost:27017/fcollection-dev-specs';
}

var config = {
    port: port,
    secret: 'xxx',
    mongoUrl: mongoUrl,
    tilda: {
        url: 'http://api.tildacdn.info/v1/',
        publickey: 'xxx',
        secretkey: 'xxx'
    },
    defaults: {
        pagination: {
            skip: 0,
            limit: 12,
            cover: {
                skip: 0,
                limit: 2
            }
        }
    }
};
module.exports = config;


require('../utils/splicePollyfill');
