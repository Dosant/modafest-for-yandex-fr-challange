var express = require('express');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');

function expressConfig(app, config) {

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(morgan('dev'));
    app.use(cookieParser());
    app.set('secret', config.secret);

}

module.exports = expressConfig;
