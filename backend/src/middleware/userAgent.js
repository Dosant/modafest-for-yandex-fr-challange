var useragent = require('useragent');

module.exports = function userAgent(req, res, next) {
	var agent = useragent.lookup(req.headers['user-agent']);
	req.isMin = checkAgent(agent);
	next();
}

// return true if agent is not fully supported
function checkAgent(agent) {

	if (agent.family === 'Android' && agent.major <= 4) {
		if (agent.minor <= 3) {
			return true
		}
	}

	if (agent.family === 'IE') {
		return true;
	}

	return false;
}
