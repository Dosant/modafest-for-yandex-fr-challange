var defaultPagination = require('../config/config').defaults.pagination;
var deepcopy = require("deepcopy");

function getDefaultPagination() {
	return deepcopy(defaultPagination);
}

function processPagination(req, res, next) {
    var pagination = getDefaultPagination();

    if (req.params.pagination) {
        if (req.params.pagination.offset !== undefined) {
            pagination.skip = +req.params.pagination.offset;
        }
        if (req.params.pagination.limit !== undefined) {
            pagination.limit = +req.params.pagination.limit;
        }
    }

    else if (req.query.page !== undefined) {
        pagination.skip = pagination.limit * (req.query.page - 1);
        pagination.cover.skip = pagination.cover.limit * (req.query.page -1);
    }

    else if (req.query.offset || req.query.limit) {
        if (req.query.offset !== undefined) {
            pagination.skip = +req.query.offset;
        }

        if (req.query.limit !== undefined) {
            pagination.limit = +req.query.limit;
        }
    }

    req.params.pagination = pagination;
    next();
}

module.exports = processPagination;
