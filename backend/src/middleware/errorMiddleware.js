module.exports = function errorMiddleware(err, req, res, next) {
    res.status(err.status || 500);
    res.json(err);
    console.error(err);
};