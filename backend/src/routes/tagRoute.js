var express = require('express');
var router = express.Router();

var tagController = require('../controllers/tagController');
var paginationMiddleware = require('../middleware/pagination');


router.route('/')
    .get(tagController.getTags);
router.route('/:id')
    .get(paginationMiddleware)
    .get(tagController.getTagById);

module.exports = router;
