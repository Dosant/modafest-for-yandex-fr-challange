var express = require('express');
var router = express.Router();

var tildaController = require('../controllers/tildaController');
router.get('/fetchTilda', tildaController.fetchTilda);
router.get('/fetchTilda/:projectId', tildaController.fetchTilda);
router.get('/page/:projectId/:pageId', tildaController.getPage);

module.exports = router;