var express = require('express');
var router = express.Router();

var utilsController = require('../controllers/utilsController');
router.get('/fetchFC/posts', utilsController.fetchFCPosts);
router.get('/fetchFC/posts/:id', utilsController.fetchFCPost);
router.post('/fetchFC/posts', utilsController.fetchFCPostsByIds);

module.exports = router;