var express = require('express');
var router = express.Router();
var articleController = require('../controllers/articleController');
var paginationMiddleware = require('../middleware/pagination');

router.route('/')
    .get(paginationMiddleware)
    .get(articleController.getArticles)
    .post(articleController.postArticle);

router.route('/news')
    .get(paginationMiddleware)
    .get(articleController.getNews);

router.route('/brand')
    .get(paginationMiddleware)
    .get(articleController.getBrand);

router.route('/lookbook')
    .get(paginationMiddleware)
    .get(articleController.getLookbook);

router.route('/:id')
    .get(articleController.getArticleById)
    .put(articleController.editArticleById)
    .delete(articleController.deleteArticleById);

router.route('/search/:term')
    .get(articleController.searchArticles);

router.route('/type')
    .post(articleController.postArticleType);

module.exports = router;
