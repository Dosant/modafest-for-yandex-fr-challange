var express = require('express');
var router = express.Router();

var imageController = require('../controllers/imageController');

router.route('/')
    .get(imageController.getImages)
    .delete(imageController.deleteImage)
    .post(imageController.postImage);

module.exports = router;
