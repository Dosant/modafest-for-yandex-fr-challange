var express = require('express');
var router = express.Router();
var sectionController = require('../controllers/sectionController');
var paginationMiddleware = require('../middleware/pagination');

router.route('/')
    .get(sectionController.getSections)
    .post(sectionController.postSection)
    .delete(function (req, res, next) {
        sectionController.resetSections()
			.then(res.json)
			.catch(next);
    });


router.route('/:id')
    .get(paginationMiddleware)
    .get(sectionController.getSectionById);

module.exports = router;
