var express = require('express');
var router = express.Router();

var userController = require('../controllers/userController');

router.post('/', userController.addUser);
router.delete('/', userController.reset);

module.exports = router;
