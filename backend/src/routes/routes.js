var express = require('express');
var router = express.Router();
var checkAuthMiddleware = require('../middleware/checkAuth');

router.use('/auth', require('./authRoute'));
router.use(checkAuthMiddleware)

router.use('/articles', require('./articleRoute'));
router.use('/sections', require('./sectionRoute'));
router.use('/tags', require('./tagRoute'));
router.use('/images', require('./imageRoute'));
router.use('/utils', require('./utilsRoute'));
router.use('/tilda', require('./tildaRoute'));
router.use(require('../middleware/errorMiddleware'));

module.exports = router;
