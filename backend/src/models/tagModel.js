var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tag = {
    "id": {type: String, required: true, lowercase: true},
    "name": {type: String, required: true},
    "articles" : {type: [Schema.Types.ObjectId], default: []},
    "count" : {type: Number, default: 0},
    "priority": {type: Number, default: 0}
};
var tagSchema = new Schema(tag);

//Export Model
module.exports = mongoose.model( 'Tag', tagSchema );
