var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var section = {
    "id": {type: String, required: true, lowercase: true},
    "name": {type: String, required: true},
	"tilda": {
		"projectId" : {type: String},
		"pageId": {type: String}
	},
	"tag": {type: String},
    "priority": {type: Number, default: 0},
    "articles" : {type: [Schema.Types.ObjectId], default: []},
    "count" : {type: Number, default: 0}
};
var sectionSchema = new Schema(section);

//Export Model
module.exports = mongoose.model( 'Section', sectionSchema );
