var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tildaProject = {
    "projectId": {type: String, required: true},
    "title": {type: String},
    "files" : {
        "css": [{type: [String], default: []}],
        "js": [{type: [String], default: []}]
    },
    "pages" : {type: [Schema.Types.Mixed], default: []}
};
var tildaProjectSchema = new Schema(tildaProject);

//Export Model
module.exports = mongoose.model( 'TildaProject', tildaProjectSchema );
