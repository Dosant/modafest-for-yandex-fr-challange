var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var imageSchema = new Schema({
    "id": { type: String, required: true },
    "url": { type: String, required: true },
    "meta": { type: Schema.Types.Mixed },
    "createdAt": { type: Date, default: Date.now }
});

//Export Model
module.exports = mongoose.model('Image', imageSchema);
