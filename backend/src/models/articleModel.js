var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Image = require('./imageModel.js');


var articalSchema = new Schema({
    "id": {
        type: String, required: true, unique: true, dropDups: true
    },
    "title": { type: String, required: true },
    "author": { type: String, required: true },
    "excerpt": { type: String, default: "" },
    "type": { type: String, enum: ["REGULAR", "NEWS", "COVER", "BRAND", "LOOKBOOK"] , default: "REGULAR"},
    "fullText": { type: String, default: "" },
	"isPublished" : {type: Boolean, default: true},
    "createdAt": { type: Date, default: Date.now },
    "modifiedAt": { type: Date, default: Date.now },
	"publishedAt": { type: Date },
    "oldId": {type: String},
    "featuredImage": Image.schema,
    "sections": [{
        _id: Schema.Types.ObjectId,
        id: String,
        name: String
    }],
    "tags": [{
        _id: Schema.Types.ObjectId,
        id: String,
        name: String
    }]
});

articalSchema.index({ id: 1 });
articalSchema.index({ createdAt: -1, type: 1 });
articalSchema.index({ 'sections._id': 1 }, { createdAt: -1 });
articalSchema.index({ 'tags._id': 1 }, { createdAt: -1 });
articalSchema.index({'title': 'text', 'excerpt': 'text'}, {"weights": { title: 3, excerpt:1 }});

var ArticleModel = mongoose.model('Article', articalSchema);

// Custom validation for unique id
ArticleModel.schema.path('id').validate(function (value, cb) {
    var self = this;
    ArticleModel.findOne({id: value}, function(err, article) {
        if (err) return cb(err);
        if (article && self._doc._id.id !== article._doc._id.id) return cb(false);
        cb(true);
    });
}, '{VALUE}: id already exists!');

//Export Model
module.exports = ArticleModel;
