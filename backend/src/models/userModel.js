var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bcrypt = require('bcrypt-nodejs');

var user = {
    "email": {
        type: String,
        required: true
    },
    "password": {
        type: String,
        required: true
    }
};
var userSchema = new Schema(user);

userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
};

userSchema.pre('save', function (next) {
    var user = this;
    if (!user.isModified('password')) return next();
    user.password = userSchema.methods.generateHash(user.password);
    next();
});

//Export Model
module.exports = mongoose.model('User', userSchema);
