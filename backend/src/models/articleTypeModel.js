var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var articleTypeSchema = new Schema({
    "id": { type: String, required: true },
	"metadata": { type: Schema.Types.Mixed }
});

module.exports = mongoose.model('ArticleType', articleTypeSchema);
