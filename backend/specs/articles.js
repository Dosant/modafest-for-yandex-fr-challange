/* eslint-env node, mocha */

module.exports = function (app, chai, should) {
    'use strict'
    const utils = require('./utils')
    const ArticleModel = require('../src/models/articleModel')

    describe('Articles', function () {
        beforeEach(function (done) {
            ArticleModel.collection.drop()
            utils.createFakeArticles(10)
                .then((res) => {
                    done()
                })
        })

        afterEach(function (done) {
            ArticleModel.collection.drop(() => {
                done()
            })
        })

        it('should get articles /articles', function (done) {
            chai.request(app)
                .get('/api/articles')
                .then((res) => {
                    res.should.have.status(200)
                    res.body.should.be.instanceof(Array)
                    res.body.length.should.equal(10)
                    done()
                })
                .catch((err) => {
                    throw err
                })
        })

        it('should get article by id /articles/:id', function (done) {
            const articleId = '1'
            chai.request(app)
                .get('/api/articles/' + articleId)
                .then((res) => {
                    res.should.have.status(200)
                    res.body.should.have.property('id')
                    res.body.should.have.property('title')
                    res.body.should.have.property('fullText')
                    res.body.id.should.equal(articleId)
                    done()
                })
                .catch((err) => {
                    throw err
                })
        })

        it('should delete article by id delete /article/:id', function (done) {
            const articleId = '1'
            chai.request(app)
                .delete('/api/articles/' + articleId)
                .then((res) => {
                    res.should.have.status(200)
                    ArticleModel.findOne({ id: articleId }, (err, result) => {
                        should.not.exist(result)
                        should.not.exist(err)
                        done()
                    })
                })
                .catch((err) => {
                    throw err
                })
        })

        it('should edit article by id put /article/:id', function (done) {
            const articleId = '1'
            const articleTitle = '1'
            const newArticleTitle = '_1'
            chai.request(app)
                .put('/api/articles/' + articleId)
                .send({ 'title': newArticleTitle })
                .then((res) => {
                    res.should.have.status(200)
                    ArticleModel.findOne({ id: articleId }, (err, result) => {
                        result.title.should.equal(newArticleTitle)
                        result.modifiedAt.should.be.above(result.createdAt)
                        done()
                    })
                })
                .catch((err) => {
                    console.log(err)
                })
        })

        it.only('should create article post article', function (done) {
            const article = {
                id: 'id',
                title: 'title',
                excerpt: 'excerpt',
                author: 'me'
            }
            return utils.login(chai.request(app))
                .then((res) => {
                    const token = res.body.token;
                    return chai.request(app)
                        .post('/api/articles')
                        .set({ 'x-access-token': token })
                        .send(article)
                        .then((res) => {
                            res.should.have.status(200)
                            ArticleModel.findOne({ id: article.id }, (err, result) => {
                                should.not.exist(err)
                                should.exist(result)
                                result.id.should.equal(article.id)
                                result.title.should.equal(article.title)
                                done()
                            })
                        })
                        .catch((err) => {
                            throw err
                        })
                })
        })

        it('should unabled to add article with existing id', function (done) {
            const article = {
                id: '1',
                title: 'title',
                excerpt: 'excerpt',
                author: 'me'
            }
            chai.request(app)
                .post('/api/articles')
                .send(article)
                .then((res) => {
                    throw res
                })
                .catch((err) => {
                    done()
                })
        })
    })
}
