'use strict';
process.env.NODE_ENV = 'test';

const app = require('../app');
const chai = require('chai');
const should = chai.should();
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

require('./articles')(app, chai, should);
require('./sections')(app, chai, should);
