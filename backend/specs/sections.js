module.exports = function (app, chai, should) {
    'use strict';
    const utils = require('./utils');
    const ArticleModel = require('../src/models/articleModel');
    const SectionModel = require('../src/models/sectionModel');
    var async = require("async");


    describe('Sections', function () {
        beforeEach(function (done) {
            SectionModel.collection.drop();
            utils.createFakeSections(5)
                .then((res) => {
                    done();
                });
        });

        afterEach(function (done) {
            SectionModel.collection.drop(() => {
                done();
            });
        });

        it('should get sections /sections', function (done) {
            chai.request(app)
                .get('/api/sections')
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.instanceof(Array);
                    res.body.length.should.equal(5);
                    // check priority sorting
                    const sorted = res.body.slice().sort((a, b) => a.priority < b.priority);
                    for (let i = 0; i < sorted.length; i++) {
                        res.body[i].id.should.be.equal(sorted[i].id);
                    }
                    done();
                })
                .catch((err) => {
                    throw err;
                });
        });

    });

    function dropModels(cb) {
        async.parallel([(cb) => {
            SectionModel.collection.drop(() => {
                cb();
            });
        },
            (cb) => {
                ArticleModel.collection.drop(() => {
                    cb();
                });
            }], () => cb());
    }


    describe('Sections & Arcticls', function () {
        beforeEach(function (done) {
            dropModels(done);
        });

        afterEach(function (done) {
            dropModels(done);
        });

        it('should create articles with sections', function (done) {
            const article1 = {
                id: 'id1',
                title: 'title',
                excerpt: 'excerpt',
                author: 'me',
                sections: [{ id: 'section1', name: 'section1' }],
                createdAt: 3,
                modefiedAt: 3,
				isPublished: true
            };

            const article2 = {
                id: 'id2',
                title: 'title',
                excerpt: 'excerpt',
                author: 'me',
                sections: [{ id: 'section2', name: 'section2' }],
                createdAt: 2,
                modefiedAt: 2,
				isPublished: true
            };

            const article3 = {
                id: 'id3',
                title: 'title',
                excerpt: 'excerpt',
                author: 'me',
                sections: [
                    { id: 'section1', name: 'section1' },
                    { id: 'section2', name: 'section2' },
                    { id: 'section3', name: 'section3' }
                ],
                createdAt: 1,
                modefiedAt: 1,
				isPublished: true
            };

            chai.request(app)
                .post('/api/articles')
                .send(article1)
                .then(res => {
                    res.should.have.status(200);
                })
                .then(() => {
                    return chai.request(app)
                        .get('/api/sections')
                        .then((res) => {
                            const body = res.body;
                            body.should.be.instanceof(Array);
                            body.length.should.equal(1);
                            body[0].id.should.equal('section1');
                        });
                })
                .then(() => {
                    return chai.request(app)
                        .post('/api/articles')
                        .send(article2);
                })
                .then(res => {
                    res.should.have.status(200);
                })
                .then(() => {
                    return chai.request(app)
                        .get('/api/sections')
                        .then((res) => {
                            const body = res.body;
                            body.should.be.instanceof(Array);
                            body.length.should.equal(2);
                            body[0].id.should.equal('section1');
                            body[1].id.should.equal('section2');
                        });
                })
                .then(() => {
                    return chai.request(app)
                        .post('/api/articles')
                        .send(article3);
                })
                .then(res => {
                    res.should.have.status(200);
                })
                .then(() => {
                    return chai.request(app)
                        .get('/api/sections')
                        .then((res) => {
                            const body = res.body;
                            body.should.be.instanceof(Array);
                            body.length.should.equal(3);
                            body[0].id.should.equal('section1');
                            body[1].id.should.equal('section2');
                            body[2].id.should.equal('section3');
                            body[0].count.should.equal(2);
                            body[1].count.should.equal(2);
                            body[2].count.should.equal(1);
                        });
                })
                .then(() => {
                    return chai.request(app)
                        .get('/api/sections/section1')
                        .then((res) => {
							var articles = res.body.articles;
                            articles.should.be.instanceof(Array);
                            articles[0].id.should.equal('id1');
                            articles[1].id.should.equal('id3');
                            done();
                        })
                        .catch((err) => console.log(err));
                });
        });
    });
};
