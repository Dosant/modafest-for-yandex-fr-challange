'use strict';

const ArticleModel = require('../src/models/articleModel');
const SectionModel = require('../src/models/sectionModel');

module.exports = {
    login,
    createFakeArticle,
    createFakeArticles,
    createFakeSection,
    createFakeSections
};

function login (request) {
    return request
        .post('/api/auth/login')
        .send({
            email: 'xxx',
            password: 'xxx'
        })
}

function createFakeArticle(id) {
    return new Promise((resolve, reject) => {
        const article = new ArticleModel({
            id: id,
            title: id,
            excerpt: id,
            author: id
        });

        article.save((err) => {
            if (err) { reject(err); }
            resolve(article);
        });
    });
}

function createFakeArticles(count) {
    let articlesPromises = [];
    for (let i = 1; i <= count; i++) {
        articlesPromises.push(createFakeArticle(i));
    }

    return Promise.all(articlesPromises);
}

function createFakeSection(id) {
    return new Promise((resolve, reject) => {
        const section = new SectionModel({
            id: id,
            name: id,
            priority: (Math.random() * 100)
        });

        section.save((err) => {
            if (err) { reject(err); }
            resolve(section);
        });
    });
}

function createFakeSections(count) {
    let sectionsPromise = [];
    for (let i = 1; i <= count; i++) {
        sectionsPromise.push(createFakeSection(i));
    }

    return Promise.all(sectionsPromise);
}