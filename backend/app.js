var express = require('express');
var app = express();
var config = require('./src/config/config');
var path = require('path');
require('./src/config/express')(app, config);

// connect to mongo instance
var mongoose = require('mongoose');
var db = mongoose.connect(config.mongoUrl);

// use middleware to add response headers (*)
var responseHeaders = require('./src/middleware/responseHeaders');
app.use(responseHeaders);

// prerender.io
app.use(require('prerender-node').set('prerenderToken', 'xxx'));

// use '/api' routes
app.use('/api', require('./src/routes/routes'));

// serve Frontend directory except index.html
app.use('/assets', express.static(path.join(__dirname, 'public/frontend/assets')));
app.use('/scripts', express.static(path.join(__dirname, 'public/frontend/scripts')));
app.use('/styles', express.static(path.join(__dirname, 'public/frontend/styles')));

// serve Frontend.min directory

app.use('/static', express.static(path.join(__dirname, 'public/frontend.min/static')));


// serve Admin directory
app.use('/admin', express.static(path.join(__dirname, 'public/admin')));

// sitemap
app.use('/sitemap.xml', express.static(path.join(__dirname, 'public/frontend/assets/meta/sitemap.xml')));

// serve frontend index
var userAgentMiddleware = require('./src/middleware/userAgent');
app.get('*', userAgentMiddleware);
app.get('*', (req, res) => {
  var frontendIndexFile;
  if (req.isMin) {
    frontendIndexFile = path.join(__dirname, 'public/frontend.min/index.html');
  } else {
    frontendIndexFile = path.join(__dirname, 'public/frontend/index.html');
  }
  res.sendFile(frontendIndexFile);
});

app.listen(config.port, function () {
  console.log('Express server listening on port ' + config.port);
});

mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open to ' + config.mongoUrl);
});

mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose disconnected on app termination');
    process.exit(0);
  });
});

module.exports = app;

