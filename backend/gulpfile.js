var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var config = require('./src/config/config');

var args = process.argv;
args.splice(0,2);

gulp.task('serve', function () {
    var jsFiles = ['*.js'];
    var options = {
        script: 'app.js',
        delayTime: 1,
        args: args,
        watch: jsFiles
    };

    return nodemon(options)
        .on('restart', function (ev) {
            console.log('Restarting....');
        });
});

gulp.task('default', ['serve']);
