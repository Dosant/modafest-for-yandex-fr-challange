'use strict';
var path = require('path');
var gulp = require('gulp');
var ngConfig = require('gulp-ng-config');
var conf = require('./conf');
var rename = require('gulp-rename');
var gutil = require('gulp-util');


gulp.task('ngConstants', [], function () {

    //check --env flag
    var args = process.argv;
    var environment = 'local';
    if (args.indexOf('--env') !== -1) {
        var index = args.indexOf('--env');
        environment = args[index + 1];
    }
    gutil.log(gutil.colors.red('Enviroment passed to Angular: ', environment));

    gulp.src('.ngConstants.json')
        .pipe(ngConfig('fcollectionAdmin', {
            environment: environment,
            createModule: false,
            wrap: true
        }))
        .pipe(rename('ngConstants.js'))
        .pipe(gulp.dest(path.join(conf.paths.src, 'app')));
});

