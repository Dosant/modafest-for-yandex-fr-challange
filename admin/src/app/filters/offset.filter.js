(function () {
    'use strict';
    
    angular.module('fcollectionAdmin')
        .filter('offset', function () {
            return function (input, start) {
                if(Array.isArray(input)){
                    start = parseInt(start, 10);
                    return input.slice(start);
                }
            };
        })
})();