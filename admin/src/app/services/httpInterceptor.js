(function () {
  'use strict';
  angular
    .module('fcollectionAdmin')
    .config(httpInterceptorConfig)

  /** @ngInject */
  function httpInterceptorConfig($httpProvider) {
    $httpProvider.interceptors.push(interceptor);
  }

  /** @ngInject */
  function interceptor($q, $injector, $log) {

    return {
      responseError: function (rejection) {
        var authService = $injector.get('authService');
        if (rejection.status === 401) {
            authService.logout();
        }    
        return $q.reject(rejection);
      }
    }
  }
})();
