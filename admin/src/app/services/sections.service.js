(function(){
    'use strict';
    angular
        .module('fcollectionAdmin')
        .factory('sections', sections);

    /* @ngInject */
    function sections(fcAdminAPI){
        return fcAdminAPI.getSections();
    }
})();
