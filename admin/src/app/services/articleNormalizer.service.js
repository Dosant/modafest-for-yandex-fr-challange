(function () {
    'use strict';
    angular
        .module('fcollectionAdmin')
        .factory('articleNormalizer', articleNormalizer);

    /* @ngInject */
    function articleNormalizer(stringTransformer, tagsService) {

        function normalize(article) {

            var normalizedArticle = {};
            normalizedArticle.title = article.title;
            normalizedArticle.excerpt = article.excerpt;
            normalizedArticle.fullText = stringTransformer.utf8_to_b64(article.fullText);
            normalizedArticle.type = article.type || 'REGULAR';


			normalizedArticle.isPublished = article.isPublished;

            normalizedArticle.author = article.author || 'Fashion Collection';
            normalizedArticle.createdAt = article.createdAt || Date.now();
            normalizedArticle.modifiedAt = normalizedArticle.createdAt;

            if (article.featuredImage) {
                normalizedArticle.featuredImage = {
                    height: article.featuredImage.height,
                    width: article.featuredImage.width,
                    name: article.featuredImage.name,
                    url: article.featuredImage.url,
                    id: article.featuredImage.id || article.featuredImage['public_id']
                };
            }

			var allTags = tagsService.getTags();

            normalizedArticle.tags = article.tags.map(function (tag) {
                if (tag.id) {
                    return tag;
                } else {
					// проверка на дибила
					var exists = allTags.find(function (_tag) {
						return _tag.name.toLowerCase() === tag.name.toLowerCase();
					});

					if (!exists) {
						var tagId = stringTransformer.unislug(tag.name);
						return {
							name: tag.name,
							id: tagId
						};
					} else {
						return exists;
					}
                }
            });
            normalizedArticle.sections = article.sections;

            var id = stringTransformer.unislug(normalizedArticle.title);
            normalizedArticle.id = id;

            return normalizedArticle;
        }

        return normalize;
    }
})();
