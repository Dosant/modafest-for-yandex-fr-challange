(function () {
    'use strict';
    angular
        .module('fcollectionAdmin')
        .factory('tags', tags);

    /* @ngInject */
    function tags(fcAdminAPI, tagsService) {
        return fcAdminAPI.getTags()
			.then(function (tags) {
				tagsService.setTags(tags);
				return tags;
			});
    }
})();


(function () {
    'use strict';
    angular
        .module('fcollectionAdmin')
        .factory('tagsService', tagsService);

    /* @ngInject */
    function tagsService() {
		var _tags
        return {
			setTags: setTags,
			getTags: getTags
		}

		function setTags(tags) {
			_tags = tags;
		}

		function getTags() {
			return _tags || [];
		}
    }
})();
