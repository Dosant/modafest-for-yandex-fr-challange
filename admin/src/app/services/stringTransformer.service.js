(function () {
  'use strict';
  angular
    .module('fcollectionAdmin')
    .factory('stringTransformer', stringTransformer);

  /* @ngInject */
  function stringTransformer() {

    function unislug(str) {
      return slug(str).toLowerCase();
    }

    function utf8_to_b64(str) {
      return btoa(unescape(encodeURIComponent(str)));
    }

    function b64_to_utf8(str) {
      return decodeURIComponent(escape(atob(str)));
    }

    return {
        unislug: unislug,
        utf8_to_b64: utf8_to_b64,
        b64_to_utf8: b64_to_utf8
    };
  }
})();
