(function () {
    'use strict';
    angular
        .module('fcollectionAdmin')
        .factory('authService', authService);

    /* @ngInject */
    function authService($state, $q, fcAdminAPI, $sessionStorage, $http, stringTransformer, $log) {


        var authService = {
            getUser: getUser,
            getUserName: getUserName,
            isLogged: getIsLogged,
            login: login,
            logout: logout,
            requireAuth: requireAuth,
            tryLoginWithSession: tryLoginWithSession
        };

        return authService;

        var currentAuth;
        //functions

        function requireAuth() {
            if (currentAuth) {
                return $q.resolve(currentAuth);
            } else {
                return $q.reject("AUTH_REQUIRED");
            }
        }

        function getIsLogged() {
            return getUser() != null;
        }

        function login(login, password) {
            return fcAdminAPI.login(login, password)
                .then(function (authData) {
                    currentAuth = getUserFromToken(authData.token);
                    $log.info('currentAuth', currentAuth);
                    $http.defaults.headers.common['x-access-token'] = authData.token;
                    saveToken(authData.token);
                    return currentAuth;
                });
        }

        function getUserFromToken(token) {
            var tokens = token.split('.');
            var user = JSON.parse(stringTransformer.b64_to_utf8(tokens[1]));
            return user;
        }

        function tryLoginWithSession() {
            var token = getToken();
            if (!token) {
                $state.go('login');
            } else {
                currentAuth = getUserFromToken(token);
                $http.defaults.headers.common['x-access-token'] = token;
            }
        }

        function logout() {
            removeToken();
            currentAuth = null;
            $state.go('login');
        }

        function getUser() {
            var token = getToken();
            if (!token) {
                return null;
            } else {
                return getUserFromToken(token);
            }
        }

        function getUserName() {
            return getUser().email;
        }

        function saveToken(token) {
            $sessionStorage.token = token;
        }

        function removeToken() {
            $sessionStorage.$reset();
        }

        function getToken() {
            return $sessionStorage.token;
        }
    }
})();
