(function () {
	'use strict'

	angular
		.module('fcollectionAdmin')
		.factory('fcImageGallery', Service)

	function Service() {
		var service = {
			generateGalleryTpl: generateGalleryTpl
		}

		return service

		////////////////
		function generateGalleryTpl(images) {
			var tpl = '<div class="fc-image-gallery">'
			return images.reduce(function (tpl, image, index) {
				return tpl + '<figure class="fc-image-gallery__thumb-figure" data-width="' + image.width + '" data-height="' + image.height + '" data-src="' + image.url + '" onclick="openGallery('+ index + ')">\
<img class="fc-image-gallery__thumb-img" src="' + image.thumbnailUrl + '" />\
</figure>'
			}, tpl) + '</div>'
		}

		/*

		<div class="fc-image-gallery">
					<figure class="fc-image-gallery__thumb-figure" data-width="1200" data-height="600" data-src="http://res.cloudinary.com/newlifemedia/image/upload/v1473954100/goldenalfa_tppclv.jpg">
					  <img class="fc-image-gallery__thumb-img" src="http://res.cloudinary.com/newlifemedia/image/upload/v1473954100/goldenalfa_tppclv.jpg" />
					</figure>

					<figure class="fc-image-gallery__thumb-figure" data-width="1000" data-height="663" data-src="http://res.cloudinary.com/newlifemedia/image/upload/v1473927784/fcollection/ekxettnktkswnd0tmqhr.jpg">
					  <img class="fc-image-gallery__thumb-img" src="http://res.cloudinary.com/newlifemedia/image/upload/v1473927784/fcollection/ekxettnktkswnd0tmqhr.jpg" />
					</figure>

				  <figure class="fc-image-gallery__thumb-figure" data-width="375" data-height="487" data-src="http://res.cloudinary.com/newlifemedia/image/upload/v1473931005/fcollection/lztri2ygs2detk0mcqor.jpg">
					  <img class="fc-image-gallery__thumb-img" src="http://res.cloudinary.com/newlifemedia/image/upload/v1473931005/fcollection/lztri2ygs2detk0mcqor.jpg" />
					</figure>

				  <figure class="fc-image-gallery__thumb-figure" data-width="1200" data-height="600" data-src="http://res.cloudinary.com/newlifemedia/image/upload/v1473954100/goldenalfa_tppclv.jpg">
					  <img class="fc-image-gallery__thumb-img" src="http://res.cloudinary.com/newlifemedia/image/upload/v1473954100/goldenalfa_tppclv.jpg" />
					</figure>

					<figure class="fc-image-gallery__thumb-figure" data-width="1000" data-height="663" data-src="http://res.cloudinary.com/newlifemedia/image/upload/v1473927784/fcollection/ekxettnktkswnd0tmqhr.jpg">
					  <img class="fc-image-gallery__thumb-img" src="http://res.cloudinary.com/newlifemedia/image/upload/v1473927784/fcollection/ekxettnktkswnd0tmqhr.jpg" />
					</figure>
				</div>

			  */
	}
})();
