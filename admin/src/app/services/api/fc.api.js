(function () {
    'use strict';
    angular
        .module('fcollectionAdmin')
        .factory('fcAdminAPI', fcAdminAPI);

    /* @ngInject */
    function fcAdminAPI($http, $log, articleNormalizer, stringTransformer, $q, api) {
		var server = api;
        var adminApi = {
            login: login,

            getTags: getTags,
            getSections: getSections,
            getArticles: getArticles,

            getArticle: getArticle,
            postArticle: postArticle,
            putArticle: putArticle,
            deleteArticle: deleteArticle,

            getImages: getImages,
            deleteImage: deleteImage

        }
        return adminApi;

        //////////

        function login(email, password) {
            return $http.post(server + '/auth/login', { email: email, password: password })
                .then(function (authData) {
                    $log.info("auth success: ", authData);
                    return authData.data;
                }, function (authError) {
                    $log.error("auth failed: ", authError);
                    return $q.reject(authError.data);
                });
        }

        function getTags() {
            return $http.get(server + '/tags').then(function (res) {
                $log.info("tags:", res.data);
                return res.data;
            });
        }

        function getSections() {
            return $http.get(server + '/sections').then(function (res) {
                $log.info("sections:", res.data);
                return res.data;
            });
        }

        function getImages(next_cursor) {
            return $http.get(server + '/images', {
                params: {
                    next_cursor: next_cursor || ''
                }
            }).then(function (res) {
                res.data.resources.forEach(function (res) {
                    res.thumbnailUrl = res.url.replace('upload/', 'upload/t_media_lib_thumb/');
                })
                $log.info("images:", res.data);
                return res.data;
            });
        }

        function deleteImage(public_id) {
            return $http.delete(server + '/images', {
                params: {
                    public_ids: public_id
                }
            })
        }

        function getArticles(limit, offset) {
			var request = server + '/articles/';
			if (limit && (offset || offset === 0)) {
				request += '?offset=' + offset +'&limit=' + limit;
			} else if (limit) {
				request += '?limit=' + limit;
			}
            return $http.get(request).then(function (res) {
                $log.info("articles:", res.data);
                return res.data;
            });
        }

        function getArticle(id) {
            return $http.get(server + '/articles/' + id).then(function (res) {
                var article = res.data;
                article.fullText = stringTransformer.b64_to_utf8(article.fullText);
                $log.info('article:', res.data);
                return article;
            });
        }

        function postArticle(article) {
            var articleToPost = articleNormalizer(article);
            return $http.post(server + '/articles', articleToPost).then(function (res) {
                return res.data;
            }, function (res) {
                return $q.reject(res.data);
            });
        }

        function putArticle(article) {
            var id = article.id;
            var articleToPut = articleNormalizer(article);
            articleToPut.id = id;
            return $http.put(server + '/articles/' + article.id, articleToPut).then(function (res) {
                return res.data;
            }, function (res) {
                return $q.reject(res.data);
            });
        }

        function deleteArticle(id) {
            return $http.delete(server + '/articles/' + id).then(function (res) {
                return res.data;
            });
        }

        function postImage(image) {
            return $http.post(server + '/images', image).then(function (res) {
                return res.data;
            });
        }
    }
})();
