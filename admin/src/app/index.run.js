(function () {
  'use strict';

  angular
    .module('fcollectionAdmin')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, amMoment, $state, authService) {
    amMoment.changeLocale('ru');
    authService.tryLoginWithSession();

    $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
      // We can catch the error thrown when the $requireAuth promise is rejected
      // and redirect the user back to the home page
      if (error === "AUTH_REQUIRED") {
        $state.go("login");
      }
    });
  }
})();
