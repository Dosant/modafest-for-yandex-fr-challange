(function () {
  "use strict";
  angular.module('fcollectionAdmin')
    .config(['$provide', function ($provide) {
      // this demonstrates how to register a new tool and add it to the default toolbar
      $provide.decorator('taOptions', ['$delegate', 'taRegisterTool','taSelection', '$uibModal', function (taOptions, taRegisterTool,taSelection, $uibModal) {
        // $delegate is the taOptions we are decorating
        // here we override the default toolbars and classes specified in taOptions.
        taOptions.toolbar = [
          ['p','h1', 'h2', 'quote', 'ul' ,'ol'],
          ['bold', 'italics', 'underline'],
          ['fcInsertImage', 'insertLink','html'],
          ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent', 'outdent'],
          ['redo', 'undo'],
          ['wordcount', 'charcount']
        ];
        taOptions.classes = {
          focussed: 'focussed',
          toolbar: 'btn-toolbar',
          toolbarGroup: 'btn-group',
          toolbarButton: 'btn btn-default',
          toolbarButtonActive: 'active',
          disabled: 'disabled',
          textEditor: 'form-control',
          htmlEditor: 'form-control'
        };

        // Custom Insert Image
        taRegisterTool('fcInsertImage', {
          iconclass: "fa fa-picture-o",
          action: function ($deferred, restoreSelection) {
            var textAngular = this;
            var modalInstance = $uibModal.open({
              templateUrl: 'app/components/chooseImageModal/chooseImageModal.html',
              size: 'lg',
              controller: 'ChooseImageModalController',
              resolve: {
                multiple: false
              }
            });

            modalInstance.result.then(function (img) {
              restoreSelection();
              textAngular.$editor()
                .wrapSelection('insertImage', img[0].url);
                //textAngular.$editor().wrapSelection('insertHtml','<p class="">ivan</p>')
              //textAngular.$editor().wrapSelection('insertHtml','<img src="http://unsplash.it/150">')
              $deferred.resolve();
            });
            return false;
          },
        });
        return taOptions; // whatever you return will be the taOptions
      }]);
    }]);
})();
