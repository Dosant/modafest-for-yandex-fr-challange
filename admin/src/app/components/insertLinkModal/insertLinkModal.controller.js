(function () {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .controller('insertLinkModalController', InsertLinkModal);

    /** @ngInject */
    function InsertLinkModal($scope, $uibModalInstance) {
		$scope.link = {
			text: '',
			url: '',
			isBlank: false
		}

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        }

		$scope.insertLink  = function () {
            $uibModalInstance.close($scope.link);
        }
    }
})();
