(function () {
    'use strict'

    angular
        .module('fcollectionAdmin')
        .controller('ChooseImageModalController', ChooseImageModalController)

    /** @ngInject */
    function ChooseImageModalController($scope, $uibModalInstance, fcAdminAPI, cloudinaryConfig, toastr, multiple) {
        var nextCursor;
        $scope.selectedImages = [];


        function reloadImages() {
            fcAdminAPI.getImages().then(function (images) {
                $scope.isLoading = false
                $scope.images = images.resources
                nextCursor = images.next_cursor
            })

            $scope.selectedImages = [];
        }

        reloadImages();

        $scope.imageSelected = function (image) {
            if (image.selected) {

                if (!multiple) {
                    $scope.selectedImages.forEach(function (image) {
                        image.selected = false;
                    })
                    $scope.selectedImages = [];
                }

                $scope.selectedImages.push(image);
            } else {
                $scope.selectedImages = $scope.selectedImages.filter(function (_image) {
                    return _image.public_id !== image.public_id;
                });
            }
        }

        $scope.chooseImagesAndClose = function () {
            $uibModalInstance.close($scope.selectedImages);
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel')
        }

        $scope.loadMore = function () {
            $scope.isLoading = true
            fcAdminAPI.getImages(nextCursor).then(function (images) {
                $scope.isLoading = false
                $scope.images = $scope.images.concat(images.resources)
                if (images.next_cursor) {
                    nextCursor = images.next_cursor
                } else {
                    $scope.end = true
                }
            })
        }

        $scope.addImage = function () {
            var config = angular.extend({}, cloudinaryConfig, { theme: 'minimal' }, { folder: 'fcollection' }, { max_file_size: 1500000 })
            cloudinary.openUploadWidget(config,
                function (error, result) {
                    if (!error) {
                        toastr.success('Изображение загружено')
                        reloadImages()
                    } else {
                        toastr.error(error.message)
                    }
                })
        }
    }
})();
