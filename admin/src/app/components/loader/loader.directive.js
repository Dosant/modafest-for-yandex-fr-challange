(function () {
    'use strict';

    angular.module('fcollectionAdmin')
        .directive('loader', loader);

    /** @ngInject */
    function loader(){
        return {
            restrict: 'E',
            templateUrl: 'app/components/loader/loader.html'
            }
        };
}());
