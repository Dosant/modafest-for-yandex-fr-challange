(function () {
  'use strict';

  angular
    .module('fcollectionAdmin')
    .directive('header', header);

  /** @ngInject */
  function header($state, $rootScope, $location) {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/header/header.tpl.html',
      controller: HeaderController,
      controllerAs: 'headerController',
      bindToController: true,
      scope: true,
      link: HeaderLink
    };

    return directive;

    /** @ngInject */
    function HeaderController($state, $location, authService) {
      // change title
      var vm = this;
      vm.isActive = isActive;
      vm.logout = logout

      function isActive(state) {
        return !($location.absUrl()
          .indexOf(state) == -1);
      }

      function logout() {
        authService.logout();
      }
    }

    function HeaderLink(scope, elem, attrs) {
      if ($location.absUrl().includes('login')) {
        elem.hide();
      }

      $rootScope.$on('$stateChangeSuccess',
        function (event, toState, toParams, fromState, fromParams) {
          if (toState.name.includes('login')) {
            elem.hide();
          } else {
            elem.show();
          }
        })
    }
  }
})();
