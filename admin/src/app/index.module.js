(function () {
	'use strict';

	angular
		.module('fcollectionAdmin', [
			'ngSanitize',
			'ui.router',
			'ngStorage',
			'angularMoment',
			'ui.bootstrap',
			'bootstrapLightbox',
			'textAngular',
			'ngTagsInput',
			'ui.ace',
			'ui.grid',
			'ui.grid.resizeColumns',
			'ui.grid.selection'
        ]);

})();
