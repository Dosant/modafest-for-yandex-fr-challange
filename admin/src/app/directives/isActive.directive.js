(function () {
    'use strict';

    angular.module('fcollectionAdmin')
        .directive('dosIsActive', dosIsActive)

    /** @ngInject */
    function dosIsActive($rootScope) {
        return {
            restrict: 'A',
            scope: {
                state: "="
            },
            link: function (scope, element, attrs) {

                $rootScope.$on('$stateChangeSuccess',
                    function (event, toState, toParams, fromState, fromParams) {

                        if (toState.name.indexOf(scope.state) != -1) {
                            element.addClass('active');
                        } else {
                            element.removeClass('active');
                        }
                    })
            }
        };
    }
})();