(function() {

    'use strict';

    angular
        .module('fcollectionAdmin')
        .directive('formInvalid', formInvalid);

    function formInvalid() {
        var directive = {
            restrict: 'E',
            scope: {
              text: '='
            },
            templateUrl: 'app/directives/form-invalid/forminvalid.html'
        };

        return directive;
    }

})();
