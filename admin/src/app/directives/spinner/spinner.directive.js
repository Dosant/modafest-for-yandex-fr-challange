(function () {
    'use strict';

    angular.module('fcollectionAdmin')
      .directive('spinner', spinner)

    /** @ngInject */
    function spinner() {
      return {
        restrict: 'EA',
        templateUrl: 'app/directives/spinner/spinner.html'
      }
    }
})();
