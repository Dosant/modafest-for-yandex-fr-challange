(function () {
  'use strict';

  angular
    .module('fcollectionAdmin')
    .config(routeConfig);



  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    var authRequired = {
      "currentAuth": ["authService", function (authService) {
        return authService.requireAuth();
      }]
    };

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/_pages/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'app/_pages/home/home.html',
        controller: 'HomeController',
        controllerAs: 'homeController',
        resolve: authRequired
      })
      .state('articles', {
        abstract: true,
        url: '/articles',
        templateUrl: 'app/_pages/articles/articles.html',
        controller: 'ArticlesController',
        controllerAs: 'articlesController',
        resolve: authRequired
      })
      .state('articles.articlesList', {
        url: '/list',
        templateUrl: 'app/_pages/articles/articlesList/articlesList.html',
        controller: 'ArticlesListController',
        controllerAs: 'articlesListController',
        resolve: authRequired
      })
      .state('articles.article', {
        url: '/article/:id',
        templateUrl: 'app/_pages/articles/article/article.html',
        controller: 'articleController',
        controllerAs: 'articleController',
        resolve: authRequired
      })
      .state('photos', {
        abstract: true,
        templateUrl: 'app/_pages/photos/photos.html',
        controller: 'PhotosController',
        controllerAs: 'photosController',
        resolve: authRequired
      })
      .state('photos.gallery', {
        url: '/gallery',
        templateUrl: 'app/_pages/photos/gallery/gallery.html',
        controller: 'GalleryController',
        controllerAs: 'galleryController',
        resolve: authRequired
      })
      .state('photos.addPhotos', {
        url: '/addPhotos',
        templateUrl: 'app/_pages/photos/addPhotos/addPhotos.html',
        controller: 'AddPhotosController',
        controllerAs: 'addPhotosController',
        resolve: authRequired
      })

    $urlRouterProvider.otherwise('/home');
  }

})();
