(function () {
  'use strict';

  angular
    .module('fcollectionAdmin')
    .config(config);

  /** @ngInject */
  function config (toastr) {

    toastr.options.timeOut = 7000;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = false;

  }

})();
