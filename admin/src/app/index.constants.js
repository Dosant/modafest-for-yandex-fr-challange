/* global toastr:false, moment:false */
(function () {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .constant('toastr', toastr)
        .constant('moment', moment)
        .constant('cloudinaryConfig', {
            "cloud_name": 'xxx',
            "upload_preset": 'xxx'
        });

})();
