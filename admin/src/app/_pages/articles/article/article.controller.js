(function () {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .controller('articleController', articleController);

    /** @ngInject */
    function articleController($stateParams, fcAdminAPI,
        $scope, $uibModal, tags, sections,
        $filter, $state, toastr, $window, url, fcImageGallery) {

        var vm = this;
        $scope.isInitilizing = true;

        ///////////////////////////
        var aceEditor;
        $scope.wisawig = '';
        $scope.aceOptions = {
            mode: 'html',
            useWrapMode: true,
            theme: 'xcode',
            require: ['ace/ext/language_tools'],
            advanced: {
                enableSnippets: true,
                enableBasicAutocompletion: true,
                enableLiveAutocompletion: true
            },
            onLoad: aceLoaded
        }

        function aceLoaded(_editor) {
            aceEditor = _editor;
        }

        $scope.insertImages = function () {
            showChooseImageModal(true)
                .result.then(function (chosedImages) {
                    var result = '';
                    chosedImages.forEach(function (image) {
                        var url = image.url;
                        var imgTag = '<img src="' + url + '" width=\"'+ image.width+'\" height=\"'+ image.height+'\" alt=""/>\n';
                        result += imgTag;
                    });

                    aceEditor.insert(result);
                    $scope.articleContent = aceEditor.getValue();
                    $scope.$applyAsync();

                });
        }

        $scope.insertLink = function () {
            showInsertLinkModal()
                .result.then(function (link) {
                    var linkTag = '<a href="' + link.url + '"' + (link.isBlank ? ' target="_blank"' : '') + '>' + link.text + '</a>';
                    aceEditor.insert(linkTag);
                    $scope.articleContent = aceEditor.getValue();
                    $scope.$applyAsync();
                });
        }

        $scope.insertGallery = function () {
            showChooseImageModal(true)
                .result.then(function (images) {
                    var tpl = fcImageGallery.generateGalleryTpl(images);
                    aceEditor.insert(tpl);
                    $scope.articleContent = aceEditor.getValue();
                    $scope.$applyAsync();
                });
        }

        ///////////////////////////

        $scope.isLoading = false;

        $scope.sections = [];
        $scope.tags = [];

        tags.then(function (tags) {
            $scope.tags = tags;
        });

        sections.then(function (sections) {
            $scope.sections = sections;
            if (!vm.article.sections || vm.article.sections.length === 0) {
                vm.article.sections = [$scope.sections[0]];
            } else {
                $scope.currentSection = $scope.sections.findIndex(function (section) {
                    return section.id === vm.article.sections[0].id;
                });
            }
        });

        $scope.sectionChoosed = function (index) {
            $scope.currentSection = index;
            vm.article.sections = [$scope.sections[index]];
        };

        $scope.loadTags = function (query) {
            return $filter('filter')($scope.tags, query, false);
        };

        $scope.chooseImage = function () {
            var modal = showChooseImageModal();
            modal.result.then(function (chosedImages) {
                vm.article.featuredImage = chosedImages[0];
            });
        };

        var id = $stateParams.id;
        $scope.id = id;
        var editMode = !!id;
        $scope.editMode = editMode

        vm.article = {
            title: "",
            excerpt: "",
            sections: [],
            tags: [],
            type: "REGULAR",
            isPublished: true
        };

        if (editMode) {
            fcAdminAPI.getArticle(id).then(function (article) {
                vm.article = article;
                $scope.articleContent = article.fullText;
                $scope.currentSection = $scope.sections.findIndex(function (section) {
                    return section.id === vm.article.sections[0].id;
                });
                $scope.isInitilizing = false;
            });
        } else {
            $scope.isInitilizing = false;
        }

        $scope.saveArticle = function () {
            if (!editMode) {
                $scope.createArticle();
            } else {
                $scope.editArticle();
            }
        }

        $scope.createArticle = function () {
            vm.article.fullText = aceEditor.getValue() || "";
            $scope.isLoading = true;
            fcAdminAPI.postArticle(vm.article).then(function () {
                $scope.isLoading = true;
                $state.reload();
                toastr.success(vm.article.title + " сохранена");
            }, function (err) {
                toastr.error(JSON.stringify(err));
            });
        };

        $scope.editArticle = function () {
            vm.article.fullText = aceEditor.getValue() || "";
            $scope.isLoading = true;
            fcAdminAPI.putArticle(vm.article).then(function () {
                $scope.isLoading = true;
                $state.reload();
                toastr.success(vm.article.title + " отредактирована");
            }, function (err) {
                toastr.error(JSON.stringify(err));
            });
        };

        $scope.deleteArticle = function () {
            $scope.isLoading = true;
            fcAdminAPI.deleteArticle(id).then(function () {
                $scope.isLoading = true;
                $state.go('articles.articlesList');
                toastr.success(vm.article.title + " удалена");
            });
        };

        $scope.previewArticle = function () {
            $window.open(url + '/a/' + vm.article.id, '_blank');
        }

        function showChooseImageModal(multiple) {
            return $uibModal.open({
                templateUrl: 'app/components/chooseImageModal/chooseImageModal.html',
                size: 'lg',
                controller: 'ChooseImageModalController',
                resolve: {
                    multiple: multiple
                }
            });
        }

        function showInsertLinkModal() {
            return $uibModal.open({
                templateUrl: 'app/components/insertLinkModal/insertLinkModal.html',
                size: 'md',
                controller: 'insertLinkModalController'
            });
        }
    }
})();
