(function () {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .controller('ArticlesListController', ArticlesListController);

    /** @ngInject */
    function ArticlesListController($stateParams, $scope, fcAdminAPI, uiGridConstants) {
        var vm = this;
		vm.articles = [];
		vm.grid = {
			data: [],
			enableFiltering: true,
			enableColumnResizing: true,
			enableRowSelection: true,
			multiSelect: false,
			enableRowHeaderSelection: false,
			columnDefs: [
				{
					field: 'id',
					name: '',
					enableFiltering: false,
					enableSorting: false,
					enableHiding: false,
					enableColumnResizing: false,
					width: '29',
					cellTemplate: '<button ui-sref="articles.article({id: grid.getCellValue(row, col)})"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>'
				},
				{
					field: 'title',
					name: 'Заголовок',
					enableHiding: false,
					width: '50%'
				},
				{
					field: 'tags',
					name: 'Теги',
					enableFiltering: false,
					enableSorting: false,
					cellTemplate: '<span><span ng-repeat="tag in grid.getCellValue(row, col) track by tag._id">{{tag.name}} </span></span>'
				},
				{
					field: 'createdAt',
					name: 'Дата Создания',
					enableFiltering: false,
					enableHiding: false,
					cellTemplate: '<span>{{grid.getCellValue(row, col) | date : "dd.MM.yyyy" }}</span>',
					sort: {
						direction: uiGridConstants.DESC
					}
				},
				{
					field: 'isPublished',
					name: 'Опубликована?',
					enableFiltering: false,
					cellTemplate: '<span>{{grid.getCellValue(row, col) ? "Да" : "Нет"}}</span>'
				},
				{
					field: 'publishedAt',
					name: 'Дата Публикации',
					enableFiltering: false,
					enableHiding: false,
					cellTemplate: '<span>{{grid.getCellValue(row, col) | date : "dd.MM.yyyy" }}</span>'
				}
			]
		};

		$scope.limit = 50;
		$scope.offset = 0;

		$scope.loadArticles = function () {
			if ($scope.isLoading) { return; }
			$scope.isLoading = true;
			fcAdminAPI.getArticles($scope.limit, $scope.offset).then(function (articles) {
				$scope.offset += articles.length;
				vm.grid.data = vm.grid.data.concat(articles);
				$scope.isLoading = false;
			});
		}

		$scope.loadArticles();
    }
})();
