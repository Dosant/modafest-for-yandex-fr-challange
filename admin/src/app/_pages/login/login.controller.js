(function () {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .controller('LoginController', LoginController);

    /** @ngInject */
    function LoginController($state, authService) {
        var vm = this;
        vm.submit = submit;
        vm.loginError = false;

        function submit(){
            vm.loginError = false;
            authService.login(vm.username, vm.password).then(function(user){
                $state.go('home');
            }).catch(function (error){
                vm.loginError = true;
            })
        }
    }
})();
