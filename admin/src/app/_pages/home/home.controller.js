(function() {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .controller('HomeController', HomeController);

    /** @ngInject */
    function HomeController($stateParams, fcAdminAPI, authService) {
        var vm = this;
        vm.username = authService.getUserName();
    }
})();
