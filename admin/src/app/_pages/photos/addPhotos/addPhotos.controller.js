(function () {
    'use strict';

    angular
        .module('fcollectionAdmin')
        .controller('AddPhotosController', AddPhotosController);

    /** @ngInject */
    function AddPhotosController(cloudinaryConfig, $log, toastr, $state, fcAdminAPI, $q) {
        var vm = this;

        var config = angular.extend({}, cloudinaryConfig ,{theme: 'minimal'}, {folder: 'fcollection'}, {max_file_size: 1500000})
        cloudinary.openUploadWidget(config,
            function (error, result) {
                if(!error) {
                    toastr.success('Images Uploaded');
                } else {
                    toastr.error(error.message);
                    $state.go('photos.gallery');
                }
            });

    }
})();
