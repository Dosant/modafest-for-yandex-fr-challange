(function () {
        'use strict';

        angular
            .module('fcollectionAdmin')
            .controller('GalleryController', GalleryController);

        /** @ngInject */
        function GalleryController($stateParams, fcAdminAPI, Lightbox,toastr, $scope) {
            $scope.isLoading = true;
            var vm = this;
			vm.images = [];
            var nextCursor;

            vm.openLightboxModal = function (index) {
                var modal = Lightbox.openModal(vm.images, index);
                modal.result.then(function (imageToDelete) {
                    $scope.isLoading = true;
                    fcAdminAPI.deleteImage(imageToDelete.public_id)
                        .then(function (res) {
                            $scope.isLoading = false;
                            var index = vm.images.findIndex(function (image) {
                                return image.public_id === imageToDelete.public_id;
                            });
                            vm.images.splice(index,1);
                            toastr.success("Image is deleted");
                        });
                });
            };

            vm.loadMore = function () {
                $scope.isLoading = true;
                fcAdminAPI.getImages(nextCursor).then(function (images) {
                    $scope.isLoading = false;
                    vm.images = vm.images.concat(images.resources);
                    if (images.next_cursor) {
                        nextCursor = images.next_cursor;
                    } else {
                        vm.end = true;
                    }
                });
            };

			vm.loadMore();
        }
    })();
