(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .config(config);

    /** @ngInject */
    function config($logProvider, $compileProvider, debugEnabled, $sceProvider, cfpLoadingBarProvider) {
        $logProvider.debugEnabled(debugEnabled);
        $compileProvider.debugInfoEnabled(debugEnabled);
        $sceProvider.enabled(false);
		cfpLoadingBarProvider.includeSpinner = false;
    }

})();
