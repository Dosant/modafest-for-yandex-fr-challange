(function () {
	'use strict';

	angular
		.module('fcollectionWeb')
		.run(runBlock);

	/** @ngInject */
	function runBlock($log, $rootScope, amMoment) {
		amMoment.changeLocale('ru');
		$rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {
			// can catch errors here
			// section is not valid
		});
	}

})();
