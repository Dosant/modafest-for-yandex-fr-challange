(function () {

    'use strict';

    angular
		.module('fcollectionWeb')
        .filter('offset', function () {
            return function (input, start) {
                if (angular.isArray(input)) {
                    start = parseInt(start, 10);
                    return input.slice(start);
                }
            };
        });

})();