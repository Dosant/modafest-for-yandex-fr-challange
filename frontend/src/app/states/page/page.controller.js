(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('PageController', PageController);

    /** @ngInject */
    function PageController($stateParams, $state) {
        var vm = this;
        var pageId = $stateParams.pageId;
		vm.template = ''

		switch (pageId) {
			case 'about':
				vm.template = '/assets/static/aboutProject.html';
				break;
			case 'about-magazine':
				vm.template = '/assets/static/aboutMagazine.html';
				break;
			case 'contacts':
				vm.template = '/assets/static/contacts.html';
				break;
			case 'where-to-find':
				vm.template = '/assets/static/whereToFind.html';
				break;
			default:
				$state.go('')
		}
    }

})();
