(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('ArticleController', ArticleController);

    /** @ngInject */
    function ArticleController(fcAPI, $stateParams, $scope, stateCacheService, $window, $timeout, $document) {

        $scope.isLoading = true;
        var vm = this;
        var articleId = $stateParams.articleId;
        vm.article = $stateParams.article;

        if (!restoreState()) {

            fcAPI.getArticle(articleId).then(function (article) {
                article.fullText = article.fullText.replace(/img *src/g, 'img data-original');
                vm.article = article;
                stateCacheService.setArticle(article);
                $scope.isLoading = false;
                afterInit();
            });

        } else {
            $scope.isLoading = false;

            afterInit();
        }
        function restoreState() {
            var article = stateCacheService.getArticle(articleId);
            if (article) { angular.extend(vm.article, article); }
            return article;
        }

        vm.recommended = [];

        function loadRecommended(tag) {
            return fcAPI.getArticlesWithTag(tag)
                .then(function (res) {
                    vm.recommended.push.apply(vm.recommended, res.cover);
                    vm.recommended.push.apply(vm.recommended, res.articles);

                    // filter current article
                    vm.recommended = vm.recommended.filter(function (article) {
                        return article.id !== articleId;
                    });
                });
        }

        function processInstagram() {
            if (vm.article.fullText.indexOf('instagram') > -1) {
                $timeout(function () {
                    $window.instgrm.Embeds.process();
                }, 1000);
            }
        }

        function processGallery() {
            if (vm.article.fullText.indexOf('fc-image-gallery') > -1) {
                $timeout(function () {
                    $scope.$root.$broadcast('process-fc-gallery');
                }, 1000);
            }
        }

        function unveilImages() {
            $timeout(function () {
                $('.article-content img[data-original]').lazyload({
                    threshold : 200
                });
            }, 0);
        }


        function afterInit() {
            loadRecommended(vm.article.tags[0].id);
            updateMeta();
            unveilImages();
            processInstagram();
            processGallery();
        }

        function updateMeta() {
            var meta = {
                title: vm.article.title,
                description: vm.article.excerpt,
            };
            if (vm.article.featuredImage) {
                meta.image = vm.article.featuredImage.url;
            }

            $scope.$root.$broadcast('set-meta', meta)
        }
    }

})();
