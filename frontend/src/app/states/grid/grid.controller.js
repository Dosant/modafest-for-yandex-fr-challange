(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('GridController', GridController);

    /** @ngInject */
    function GridController(fcAPI, $stateParams, $scope, sectionsService, tildaService, stateCacheService, $q) {
		var vm = this;
        $scope.isLoading = true;

        var sectionId = $stateParams.sectionId;
        var tagId = $stateParams.tagId;
        var isSection = !!sectionId;
        var section = {};

        if (isSection) {
            section = sectionsService.getSection(sectionId);
			$scope.section = section;
			if (section.tilda) {
				var projectId = section.tilda.projectId;
				var pageId = section.tilda.pageId;
				$scope.isLoadingTilda = true;
				tildaService.getTildaPage(projectId, pageId)
					.then(function (page) {
						vm.tilda = page.html;
						$scope.isLoadingTilda = false;
					})
			}
        }

        vm.page = {
            currentPage: 1,
            totalLoaded: 0,
            totalInSection: 1,
            isLoadingMore: false,
			isEmptyFlag: true
        };

        vm.blocks = []; // 2 + 12
        vm.news = [];
        vm.brand = {
            articles: []
        };


		if (!restoreFromCache()) {
			loadArticles();
		} else {
			$scope.isLoading = false;
		}

        function loadArticles() {
            var currentPage = vm.page.currentPage;
            if (vm.page.totalLoaded !== 0) {
                vm.page.isLoadingMore = true;
            }
            if (sectionId) {
                // tagged sections: modafest, fc.academy will show articles with tag: 'modafest, fc.academy'
                if (section.tag) {
                    fcAPI.getArticlesWithTag(section.tag, currentPage)
						.then(loadArticlesSuccess, loadArticlesFail)
						.then(function () {
							stateCacheService.setSection(sectionId, vm);
						});

                } else {
                    var loadBlocks = fcAPI.getArticlesWithSection(sectionId, currentPage).then(loadArticlesSuccess, loadArticlesFail)
                    var loadNews = fcAPI.getNews().then(function (news) {
                        vm.news = news;
                    });
                    var loadBrand = fcAPI.getBrand().then(function (brand) {
                        vm.brand = brand;
                    });

                    var loadLookbook = fcAPI.getLookbook().then(function (lookbook) {
                        vm.lookbook = lookbook;
                        vm.lookbook.articles = vm.lookbook.articles
                    });

                    $q.all([loadBlocks, loadNews, loadBrand, loadLookbook])
                        .then(function () {
							stateCacheService.setSection(sectionId, vm);
						});
                }

            } else if (tagId) {
                fcAPI.getArticlesWithTag(tagId, currentPage).then(loadArticlesSuccess, loadArticlesFail)
					.then(function () {
						stateCacheService.setTag(tagId, vm);
					});
            }

            function loadArticlesSuccess(results) {
				vm.page.currentPage++;

				if (results.cover && results.cover.length === 2) {
					vm.blocks.push({
						cover: results.cover.splice(0, 1),
						articles: results.articles.splice(0, 6)
					})
				}

                vm.blocks.push(results);
                if (vm.page.isEmptyFlag && (results.articles.length > 0 || results.cover.length > 0)) {
                    vm.page.isEmptyFlag = false;
                }
                $scope.isLoading = false;
                vm.page.totalInSection = +results.total;
                vm.page.totalLoaded += results.articles.length;
                vm.page.totalLoaded += results.cover.length;
                vm.page.isLoadingMore = false;
            }

			function loadArticlesFail(err) {
				$scope.isLoading = false;
				$scope.error = err;
			}
        }

        $scope.loadMore = function loadMore() {
            loadArticles();
        }

        $scope.isEmpty = function isEmpty() {
            return vm.page.isEmptyFlag || $scope.error;
        }

		$scope.isPageLoading = function isLoading() {
			return $scope.isLoading || $scope.isLoadingTilda;
		}

        $scope.isLastPage = function isLastPage() {
            if (vm.page.isEmptyFlag) {
                return true;
            }
            return vm.page.totalLoaded >= vm.page.totalInSection;
        }

		$scope.getImageBackground = function getImageBackground(img) {
			return img ? { 'background-image': 'url(' + img + ')' } : {};
        };

		function restoreFromCache() {
			var state
			if (isSection) {
				state = stateCacheService.getSection(sectionId);
			} else {
				state = stateCacheService.getTag(tagId);
			}
			if (state) { angular.extend(vm, state) }
			return state;
		}
    }

})();
