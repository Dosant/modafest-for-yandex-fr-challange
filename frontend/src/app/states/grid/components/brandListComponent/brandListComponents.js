(function () {
    'use strict';

    var brandList = {
        bindings : {
            brand: '<'
        },
        templateUrl: 'app/states/grid/components/brandListComponent/brandListComponent.tpl.html',
        controller: BrandListController
    };

    /*ngInject*/
    function BrandListController() {
        var vm = this;
        vm.getImageBackground = function getImageBackground(img) {
			return img ? { 'background-image': 'url(' + img + ')' } : {};
        };
    }

    angular
        .module('fcollectionWeb')
        .component('brandList', brandList);

})();