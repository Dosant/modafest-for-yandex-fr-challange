(function () {
    'use strict';

    var lookbookListComponent = {
        bindings : {
            lookbook: '<'
        },
        templateUrl: 'app/states/grid/components/lookbookListComponent/lookbookListComponent.tpl.html'
    };

    angular
        .module('fcollectionWeb')
        .component('lookbookList', lookbookListComponent);

})();