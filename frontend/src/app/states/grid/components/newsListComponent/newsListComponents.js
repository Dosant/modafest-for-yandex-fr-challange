(function () {
    'use strict';

    var newsListComponent = {
        bindings : {
            news: '<'
        },
        templateUrl: 'app/states/grid/components/newsListComponent/newsListComponent.tpl.html'
    };

    angular
        .module('fcollectionWeb')
        .component('newsList', newsListComponent);

})();