(function() {

    'use strict';

    angular
        .module('fcollectionWeb')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController($scope, $window) {
		var vm = this
        $scope.openBlank = function (url) {
            $window.open(url, '_blank');
        };
		vm.isSearch = false;
    }
})();
