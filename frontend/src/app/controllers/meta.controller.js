(function() {
'use strict';

	angular
		.module('fcollectionWeb')
		.controller('MetaController', MetaController);

	/*ngInject*/
	function MetaController($scope, $log, $location) {
		var vm = this;

		$scope.$on('set-meta', function (evt, newMeta) {
			setMeta(newMeta);
		})

		$scope.$on('reset-meta', function() {
			resetMeta();
		})

		$scope.$on('$stateChangeStart', function() {
			resetMeta();
		});

		function setMeta (meta) {
			$log.info('new meta:', meta);
			angular.extend(vm, meta);
            vm.url = $location.absUrl();
		}

		function resetMeta () {
			for (var key in vm) {
				vm[key] = undefined;
			}
		}
	}
})();