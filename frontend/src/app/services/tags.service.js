(function(){

    'use strict';

    angular
        .module('fcollectionWeb')
        .factory('tags', tags);

    /* @ngInject */
    function tags(fcAPI){
        return fcAPI.getTags();
    }

})();
