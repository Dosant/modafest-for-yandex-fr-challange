(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .factory('tildaService', tildaService);

    /* @ngInject */
    function tildaService(fcAPI, $ocLazyLoad) {
		return {
			getTildaPage: getTildaPage
		}

		function getTildaPage(projectId, pageId) {
			return fcAPI.getTildaPage(projectId, pageId)
				.then(function (tilda) {
					var js = tilda.files.js;
					var css = tilda.files.css;
					var page = tilda.page;
					var jsFiles = js.map(function (js) {
						return { type: 'js', path: js }
					});
					var cssFiles = css.map(function (css) {
						return { type: 'css', path: css }
					});

					// remove jquery
					jsFiles = jsFiles.filter(function (js) {
						return js.path.indexOf('jquery-') === -1;
					});

					return $ocLazyLoad.load([
						{
							series: true,
							files: jsFiles
						}, {
							insertBefore: '#load-tilda-css-before',
							files: cssFiles
						}
					])
						.then(function () {
							return page;
						});
				})
		}
	}

})();
