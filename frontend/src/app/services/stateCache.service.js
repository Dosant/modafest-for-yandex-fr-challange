(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .factory('stateCacheService', stateCacheService);

    /* @ngInject */
    function stateCacheService() {

		var state = {
			articles: {},
			sections: {},
			tags: {}
		};

		return {
			setArticle: setArticle,
			setSection: setSection,
			setTag: setTag,
			getArticle: getArticle,
			getSection: getSection,
			getTag: getTag
		}

		function setArticle(article) {
			state.articles[article.id] = article;
			return article;
		}

		function setSection(sectionId, sectionState) {
			if (state.sections[sectionId]) {
				state.sections[sectionId] = {};
			}
			state.sections[sectionId] = sectionState
			return state.sections[sectionId];
		}

		function setTag(tagId, tagState) {
			if (state.tags[tagId]) {
				state.tags[tagId] = {};
			}
			state.tags[tagId] = tagState
			return state.tags[tagId];
		}

		function getArticle(id) {
			return state.articles[id];
		}

		function getSection(sectionId) {
			return state.sections[sectionId];
		}

		function getTag(tagId) {
			return state.tags[tagId];
		}
    }

})();
