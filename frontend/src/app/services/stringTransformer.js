(function () {

  'use strict';

  angular
    .module('fcollectionWeb')
    .factory('stringTransformer', stringTransformer);

  /* @ngInject */
  function stringTransformer() {

    function utf8_to_b64(str) {
      return btoa(unescape(encodeURIComponent(str)));
    }

    function b64_to_utf8(str) {
      return decodeURIComponent(escape(atob(str)));
    }

    return {
      utf8_to_b64: utf8_to_b64,
      b64_to_utf8: b64_to_utf8
    };
  }
})();
