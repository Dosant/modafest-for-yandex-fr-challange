(function(){

    'use strict';

    angular
        .module('fcollectionWeb')
        .factory('sections', sections);

    /* @ngInject */
    function sections(fcAPI){
        return fcAPI.getSections();
    }
})();


(function(){

    'use strict';

    angular
        .module('fcollectionWeb')
        .factory('sectionsService', sectionsService);

    /* @ngInject */
    function sectionsService(sections, $q) {
        var _sections = [];

        return {
            isSectionExists: isSectionExists,
            getSections: getSections,
            getSection: getSection
        };

        function isSectionExists(sectionId) {
            return sections.then(function (sections) {
                _sections = sections;
                var section = sections.filter(function (section) {
                    return section.id === sectionId;
                });
                return section.length > 0 ? $q.resolve(section[0]) : $q.reject(sectionId + 'doesnt exists');
            })
        }

        function getSections() {
            return _sections;
        }

        function getSection(sectionId) {
            var section = _sections.filter(function (section) {
                return section.id === sectionId;
            });
            return section.length > 0 ? section[0] : false;
        }
    }
})();

