(function () {

	'use strict';

	angular
		.module('fcollectionWeb')
		.factory('fcAPI', fcAPI);

	/* @ngInject */
	function fcAPI($http, $log, stringTransformer, api) {
		var serverUrl = api;
		var fcAPI = {
			getArticle: getArticle,
			getArticlesWithTag: getArticlesWithTag,
			getArticlesWithSection: getArticlesWithSection,
			getNews: getNews,
			getBrand: getBrand,
            getLookbook: getLookbook,
			getTags: getTags,
			getSections: getSections,
			getTildaPage: getTildaPage,
			searchArticles: searchArticles
		};

		return fcAPI;

		//////////

		function getArticle(id) {
			return $http.get(serverUrl + '/articles/' + id)
				.then(function (response) {
					var article = response.data;
					$log.info("article:", article);
					// convert fulltext
					var fullText = article.fullText;
					article.fullText = stringTransformer.b64_to_utf8(fullText);
					return article;
				});
		}

		function getArticlesWithSection(sectionId, page) {
			return $http.get(serverUrl + '/sections/' + sectionId, {
				params: {
					page: page
				}
			}).then(function (results) {
				var articles = results.data;
				var total = results.headers('total');
				$log.info(sectionId, articles);
				articles.total = total;
				return articles;
			});
		}

		function getArticlesWithTag(tagId, page) {
			return $http.get(serverUrl + '/tags/' + tagId, {
				params: {
					page: page
				}
			}).then(function (results) {
				var articles = results.data;
				var total = results.headers('total');
				$log.info(tagId, articles);
				articles.total = total;
				return articles;
			});
		}

		function getNews() {
			return $http.get(serverUrl + '/articles/news?limit=3')
				.then(function (res){
					$log.info('NEWS:', res.data)
					return res.data;
				});
		}

		function getBrand() {
			return $http.get(serverUrl + '/articles/brand?limit=3')
				.then(function (res) {
					$log.info('BRAND:', res.data)
					return res.data;
				});
		}

        function getLookbook() {
			return $http.get(serverUrl + '/articles/lookbook?limit=4')
				.then(function (res) {
					$log.info('LOOKBOOK:', res.data)
					return res.data;
				});
		}

		function getSections() {
			return $http.get(serverUrl + '/sections')
				.then(function (response) {
					var sections = response.data;
					$log.info("sections:", sections);
					return sections;
				});
		}

		function getTags() {
			return $http.get(serverUrl + '/tags')
				.then(function (response) {
					var tags = response.data;
					$log.info("tags:", tags);
					return tags;
				});
		}

		function getTildaPage(projectId, pageId) {
			return $http.get(serverUrl + '/tilda/page/' + projectId + '/' + pageId)
				.then(function (response) {
					return response.data;
				});
		}

		function searchArticles(term) {
			return $http.get(serverUrl + '/articles/search/' + term)
				.then(function (response) {
					return response.data;
				});
		}

	}

})();
