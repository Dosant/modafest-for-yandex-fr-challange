(function () {

    'use strict';

    angular
		.module('fcollectionWeb')
        .directive('fcCarousel', fcCarousel);

    /** @ngInject */
    function fcCarousel() {
        return {
            scope: {
                articles: '='
            },
            restrict: 'A',
            templateUrl: 'app/components/carousel/fcCarousel.html',
            controller: fcCarouselController,
            link: function (scope, element, attrs) {

            }
        };
    }

    /** @ngInject */
    function fcCarouselController($scope) {

        $scope.currentIndex = 0;
        var articleCount = $scope.articles.length;

        $scope.loaded = false;

        $scope.imageLoaded = function (index) {
            if (index == $scope.currentIndex) {
                $scope.loaded = true;
            }
        };

        $scope.nextArticle = function () {
            articleCount = $scope.articles.length;
            $scope.currentIndex = ($scope.currentIndex + 1) < articleCount
				? ($scope.currentIndex + 1)
				: 0;
        };

        $scope.prevArticle = function () {
            articleCount = $scope.articles.length;
            $scope.currentIndex = ($scope.currentIndex - 1) >= 0
				? ($scope.currentIndex - 1)
				: (articleCount - 1);
        };

        $scope.goToPage = function (index) {
            $scope.currentIndex = index;
        }
    }
} ());
