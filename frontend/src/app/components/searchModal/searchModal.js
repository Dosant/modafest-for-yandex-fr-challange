(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.controller('searchModalController', function ($scope, $rootScope, close) {
			$scope.close = function () {
				$rootScope.isSearchModalOpen = false;
				return close();
			};
			$scope.loading = true;
			$scope.articles = [];
			$scope.error = false;
			$scope.$watchCollection('articles', function (newValue, oldValue) {
				console.log('collection');
				if (newValue.length === 0) {
					$scope.error = true;
				} else {
					$scope.error = false;
				}
			})
			$rootScope.isSearchModalOpen = true;
		});
})();