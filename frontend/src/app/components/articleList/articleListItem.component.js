(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.component('articleListItem', {
			templateUrl: 'app/components/articleList/articleListItem.component.html',
			bindings: {
				'article': '<'
			},
			controller: articleListItemController,
			controllerAs: 'listItem'
		});

	/*@ngInject*/
	function articleListItemController() {
		this.getImageBackground = function getImageBackground(img) {
			return img ? { 'background-image': 'url(' + img + ')' } : {};
        };
	}

})();