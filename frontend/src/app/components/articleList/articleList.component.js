(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.component('articleList', {
			templateUrl: 'app/components/articleList/articleList.component.html',
			bindings: {
				'articles': '<'
			},
			controller: articleListController,
			controllerAs: 'list'
		});

	/*@ngInject*/
	function articleListController() {

	}

})();