(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.component('shareArticle', {
			templateUrl: 'app/components/shareArticle/shareArticle.component.html',
			bindings: {
				'article': '<'
			},
			controller: shareArticleController,
			controllerAs: 'share'
		});

	/*@ngInject*/
	function shareArticleController(fcAPI, $scope, $location) {
		$scope.tagsToString = function (tags) {
			if (tags.length === 0) {return '';}
			if (tags.length === 1) {return tags[0].name;}

			return tags.map(function (tag) {
				return tag.name
			}).join(', ');
		}

		$scope.getCurrentUrl = function () {
			return $location.absUrl();
		}

		$scope.getCoverImage = function () {
			return $location.protocol() + '://' + $location.host() + '/assets/meta/cover.jpg';
		}
	}

})();