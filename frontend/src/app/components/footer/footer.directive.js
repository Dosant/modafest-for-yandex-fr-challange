(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .directive('mainFooter', mainFooter);

    /** @ngInject */
    function mainFooter() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/components/footer/footer.html',
            controller: FooterController,
            controllerAs: 'footerCtrl',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function FooterController(fcAPI, social, $scope) {
            $scope.social = social;

            var vm = this;
            vm.tags = [];
            fcAPI.getTags().then(function (tags) {
                vm.tags = tags;
            });
        }
    }

})();
