(function () {
	'use strict';

	angular
		.module('fcollectionWeb')
		.directive('fcImageGallery', FCImageGallery);

	/* ngInject */
	function FCImageGallery($window) {
		// Usage:
		//
		// Creates:
		//
		var directive = {
			bindToController: true,
			controller: FCImageGalleryController,
			controllerAs: 'vm',
			link: link,
			restrict: 'E',
			templateUrl: 'app/components/fcImageGallery/fcImageGallery.html'
		};
		return directive;

		function link(scope, element, attrs) {
			var pswp = element.find('.pswp')[0];
			scope.$on('process-fc-gallery', function () {
				// process
				var imageData = $('.fc-image-gallery').children()
					.map(function () {
						return {
							src: this.getAttribute('data-src'),
							// msrc: this.children[0].getAttribute('src'),
							w: this.getAttribute('data-width'),
							h: this.getAttribute('data-height')
						}
					});

				$window.openGallery = function (idx) {
					var options = {
						index: idx,
						history: false,
						closeOnScroll: false,
						bgOpacity: 0.85
					};
					var gallery = new PhotoSwipe(pswp, PhotoSwipeUI_Default, imageData, options);
					gallery.init();
				}
			});
		}
	}
	/* @ngInject */
	function FCImageGalleryController() {

	}
})();