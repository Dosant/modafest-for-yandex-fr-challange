(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.component('mainNavBar', {
			templateUrl: 'app/components/navbar/mainnav.component/mainnavBar.component.html',
			bindings: {
				sections: '<',
				openSubnav: '<',
				openSearchBar: '<',
				toggleSubnav: '&',
				toggleSearchBar: '&'
			}
		});
})();