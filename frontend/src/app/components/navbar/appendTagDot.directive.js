(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .directive('appendTagDot', appendTagDot);

    /** @ngInject */
    function appendTagDot() {
        return function(scope, elem) {
            if (!scope.$last) {
                var dot = angular.element('<div class="sub-nav-tag">\
                                <i class="icon-dot"></i>\
                            </div>');
                elem.after(dot);
            }
        };
    }

})();
