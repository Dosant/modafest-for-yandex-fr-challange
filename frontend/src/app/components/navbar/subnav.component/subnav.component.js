(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.component('subNav', {
			templateUrl: 'app/components/navbar/subnav.component/subnav.component.html',
			bindings: {
				sections: '<',
				tags: '<'
			}
		});
})();