(function () {
    'use strict';
	angular
		.module('fcollectionWeb')
		.directive('searchInputControl', searchInputControl);

    /** @ngInject */
    function searchInputControl($window, $document) {
		return {
			restrict: 'A',
			link: function (scope, element, attrs) {
				var desktop = element[0].getElementsByClassName('desktop-search-input')[0];
				var mobile = element[0].getElementsByClassName('mobile-search-input')[0];

				function focus() {
					$window.innerWidth > 768 ? desktop.focus() : mobile.focus();
				}

				scope.$watch('searchbar.isOpen', function (newValue, oldValue) {
					if (newValue && newValue !== oldValue) {
						focus();
					}
				});

				$document.bind('keydown keypress', function (event) {
					// entr
					if(event.which === 13 && scope.searchbar.isOpen && scope.searchbar.term.length) {
						scope.searchbar.search();
					}
					// esc

					if (event.which === 27 && scope.searchbar.isOpen) {
						scope.searchbar.isOpen = false;
						scope.$apply();
					}
				});
			}
		}
    }

})();