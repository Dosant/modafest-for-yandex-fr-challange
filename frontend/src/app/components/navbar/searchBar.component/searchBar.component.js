(function () {
	'use strict';
    angular
        .module('fcollectionWeb')
		.component('searchBar', {
			templateUrl: 'app/components/navbar/searchBar.component/searchBar.component.html',
			bindings: {
				'isOpen': '=',
				'toggleSearchBar': '&'
			},
			controller: searchBarController,
			controllerAs: 'searchbar'
		});

	/*@ngInject*/
	function searchBarController(fcAPI, ModalService, $scope, $timeout) {

		var vm = this;
		vm.term = '';
		vm.search = search;
		vm.loading = false;
		var modal;
		$scope.$watch('searchbar.isOpen', function (newValue, oldValue) {
			if (newValue && newValue !== oldValue) {

			} else if (!newValue && newValue !== oldValue) {

				if (modal) {
					modal.scope.close();
					modal = undefined
				}
				$timeout(function () {
					vm.term = '';
				}, 1000);
			}
		});

		function search() {
			if (vm.loading) { return; }
			vm.loading = true;
			if (modal) {
				modal.scope.loading = true;
			} else {
				ModalService.showModal({
					templateUrl: 'app/components/searchModal/searchModal.html',
					controller: 'searchModalController'
				})
					.then(function (_modal) {
						modal = _modal;
					});
			}

			fcAPI.searchArticles(vm.term).then(function (articles) {
				vm.loading = false;
				try {
					if (angular.isArray(articles)) {
						modal.scope.articles = articles;
					} else {
						modal.scope.articles = [];
					}
					modal.scope.loading = false;
				} catch (err) {
					modal.scope.loading = false;
					console.log(err);
				}
			}, function (err) {
				console.log(err);
				vm.loading = false;
				modal.scope.articles = [];
				modal.scope.loading = false;
			});
		}
	}

})();