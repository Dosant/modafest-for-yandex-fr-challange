(function () {

    'use strict';

	angular
		.module('fcollectionWeb')
		.directive('imageLoaded', imageLoaded);

    /** @ngInject */
    function imageLoaded() {
      return {
        restrict: 'A',
        link: function (scope, element, attrs) {
          element.bind('load', function () {
            //call the function that was passed            
            scope.$apply(attrs.imageLoaded);
          });
        }
      }
    }

})();
