(function () {

    'use strict';

    angular
        .module('fcollectionWeb')
        .directive('bindHtmlCompile', bindHtmlCompile);

    /** @ngInject */
    function bindHtmlCompile($sanitize){
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                scope.$watch(function () {
                    return scope.$eval(attrs.bindHtmlCompile);
                }, function (value) {
                    if(!value)
                        return;
                    //var d = angular.element('<textarea />').html(value).text()
                    var san = $sanitize(value);
                    element.html(san);

                });
            }
        };
    }

})();
