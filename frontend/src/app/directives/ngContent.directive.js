(function () {

    'use strict';

	angular
		.module('fcollectionWeb')
		.directive('ngContent', ngContent);

    /** @ngInject */
    function ngContent() {
      return {
        scope: {
            ngContent: '='
        },
        restrict: 'A',
        link: function (scope, element, attrs) {
            var defaultContent = attrs.content || '';
            if (scope.ngContent) {
                element.attr('content', scope.ngContent);
            }

            scope.$watch('ngContent', function (newVal, oldVal) {
                if (newVal !== oldVal) {
                    if (newVal) {
                        element.attr('content', newVal);
                    } else {
                        element.attr('content', defaultContent);
                    }
                }
            });
        }
      }
    }

})();