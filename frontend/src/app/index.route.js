(function () {
	'use strict';

	angular
		.module('fcollectionWeb')
		.config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
		$locationProvider.html5Mode(true);

		$stateProvider
			.state('article', {
				url: '/a/:articleId',
				params: {
					article: {}
				},
				templateUrl: 'app/states/article/article.html',
				controller: 'ArticleController',
				controllerAs: 'articleController'
			})
			.state('section', {
				url: '/:sectionId',
				params: {
					sectionId: 'magazine'
				},
				templateUrl: 'app/states/grid/grid.html',
				controller: 'GridController',
				controllerAs: 'gridController',
				resolve: {
					isSectionExist: function (sectionsService, $stateParams) {
						return sectionsService.isSectionExists($stateParams.sectionId);
					}
				}
			})
			.state('tag', {
				url: '/t/:tagId',
				templateUrl: 'app/states/grid/grid.html',
				controller: 'GridController',
				controllerAs: 'gridController'
			})
			.state('page', {
				url: '/p/:pageId',
				templateUrl: 'app/states/page/page.html',
				controller: 'PageController',
				controllerAs: 'pageController'
			});
		$urlRouterProvider.otherwise('/');
	}

})();
