(function () {

    'use strict';

    angular
        .module('fcollectionWeb', [
            //'ngSanitize',
            'ui.router',
            'angularMoment',
            'angular-click-outside',
            'oc.lazyLoad',
            'angular-loading-bar',
            'angularModalService',
            '720kb.socialshare',
            'angulartics',
            'angulartics.google.analytics',
            'angularLazyImg'
        ]);

})();
